/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "mastercontrol.h"

class SceneObject: public LogicComponent
{
    DRY_OBJECT(SceneObject, LogicComponent);

public:
    SceneObject(Context* context);

    void OnNodeSet(Node* node) override;
    virtual void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY);
    virtual void Disable();

    Vector3 GetWorldPosition() const;

protected:
    void PlaySample(String sampleName, float frequency = 0.f, float gain = .3f, bool onNode = true);

    float randomizer_;
};

#endif // SCENEOBJECT_H

