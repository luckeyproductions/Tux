/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../creatures/creature.h"
#include "wizardhat.h"

#include "fireball.h"

Fireball::Fireball(Context* context): SceneObject(context),
    rigidBody_{ nullptr },
    graphicsNode_{ nullptr },
    fireEmitter_{ nullptr },
    sparkEmitter_{ nullptr },
    light_{ nullptr },
    age_{ 0.f }
{
}

void Fireball::OnNodeSet(Node* node)
{
    if (!node)
        return;

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(.05f);
    rigidBody_->SetRestitution(10.f);
    rigidBody_->SetFriction(0.f);
    rigidBody_->SetRollingFriction(0.f);
    rigidBody_->SetLinearDamping(.05f);
    rigidBody_->SetAngularFactor(Vector3::ZERO);
    rigidBody_->SetCollisionLayer(LAYER(L_PROJECTILE));
    rigidBody_->SetCollisionMask(LAYER(L_STATIC) | LAYER(L_ENEMY));
    rigidBody_->SetCcdMotionThreshold(12.f);
    rigidBody_->SetCcdRadius(.17f);

    CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
    collider->SetSphere(.42f);

    graphicsNode_ = node_->CreateChild("Graphics");
    graphicsNode_->Translate(Vector3::RIGHT * .023f);
    fireEmitter_ = graphicsNode_->CreateComponent<ParticleEmitter>();
    fireEmitter_->SetSorted(true);
    fireEmitter_->SetEffect(RES(ParticleEffect, "Particles/Fire.xml")->Clone());

    sparkEmitter_ = graphicsNode_->CreateComponent<ParticleEmitter>();
    sparkEmitter_->SetSorted(true);
    sparkEmitter_->SetEffect(RES(ParticleEffect, "Particles/Sparks.xml")->Clone());

    Node* lightNode{ graphicsNode_->CreateChild("Light") };
    lightNode->Translate(Vector3::BACK * .23f);
    light_ = lightNode->CreateComponent<Light>();
    light_->SetRange(1.7f);
    light_->SetColor(Color::ORANGE);

    SubscribeToEvent(node_, E_NODECOLLISIONEND, DRY_HANDLER(Fireball, HandleNodeCollisionEnd));
}

void Fireball::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    age_ = 0.f;
    velocity_ = 0.f;

    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    const Ray ray{ node_->GetWorldPosition() - node_->GetWorldDirection() * .55f, node_->GetWorldDirection() };
    PhysicsRaycastResult result{};
    if (physics->RaycastSingle(result, ray, .55f, rigidBody_->GetCollisionMask()))
        node_->SetWorldPosition(result.position_ - 0.23f * ray.direction_);

    rigidBody_->ResetForces();
    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->SetAngularVelocity(Vector3::ZERO);
    rigidBody_->ApplyWorldTransform(node_->GetWorldPosition(), node_->GetWorldRotation());
    rigidBody_->SetCollisionMask(LAYER(L_STATIC) | LAYER(L_ENEMY));
    rigidBody_->ReAddBodyToWorld();
    rigidBody_->SetEnabled(true);
    rigidBody_->ApplyImpulse(node_->GetWorldDirection() * .55f);

    const Vector3 d{ Vector3::FORWARD };
    fireEmitter_->GetEffect()->SetMinDirection(d * .25f);
    fireEmitter_->GetEffect()->SetMaxDirection(d * .5f);
    fireEmitter_->RemoveAllParticles();
    fireEmitter_->SetEmitting(true);
    sparkEmitter_->RemoveAllParticles();
    sparkEmitter_->SetEmitting(true);

    light_->SetBrightness(1.f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Fireball, HandleNodeCollisionStart));
}

void Fireball::Update(float timeStep)
{
    age_ += timeStep;

    if (age_ > 3.4f)
    {
        Disable();
    }
    else if (age_ > 1.7f)
    {
        fireEmitter_->SetEmitting(false);
        sparkEmitter_->SetEmitting(false);
        light_->SetBrightness(Clamp(PowN(1.f - 2.f * (age_ - 1.7f), 3), 0.f, 1.f));
        DisablePhysics();
    }

    graphicsNode_->RotateAround(node_->GetWorldPosition(), { 2048.f * timeStep, graphicsNode_->GetWorldDirection() }, TS_WORLD);
    graphicsNode_->SetWorldDirection(rigidBody_->GetLinearVelocity().Normalized());
}

void Fireball::FixedUpdate(float /*timeStep*/)
{
    if (!rigidBody_->IsEnabled())
        return;

    const Vector3 lv{ rigidBody_->GetLinearVelocity() };
    PODVector<RigidBody*> result{};
    if (velocity_ == 0.f || !GetScene()->GetComponent<PhysicsWorld>()->GetCollidingBodies(result, rigidBody_))
        velocity_ = Min(11.f, lv.Length());

    const Vector3 dir{ node_->GetWorldRotation().Inverse() * lv.Normalized() };
    fireEmitter_->GetEffect()->SetMinDirection(dir);
    fireEmitter_->GetEffect()->SetMaxDirection(dir);
    fireEmitter_->GetEffect()->SetMinVelocity(velocity_ * .05f);
    fireEmitter_->GetEffect()->SetMaxVelocity(velocity_ * .1f);
    sparkEmitter_->GetEffect()->SetMinDirection(dir);
    sparkEmitter_->GetEffect()->SetMaxDirection(dir);
    sparkEmitter_->GetEffect()->SetMinVelocity(velocity_ * .125f);
    sparkEmitter_->GetEffect()->SetMaxVelocity(velocity_ * .25f);
}

void Fireball::DisablePhysics()
{
    rigidBody_->SetEnabled(false);
    UnsubscribeFromEvent(E_NODECOLLISIONSTART);
}


void Fireball::HandleNodeCollisionStart(StringHash /*eventType*/, VariantMap& eventData)
{
    if (age_ >= 1.7f)
        return;

    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    if (otherNode)
    {
        if (Frost* frost{ otherNode->GetComponent<Frost>() })
            frost->Thaw();

        if (Creature* creature{ otherNode->GetDerivedComponent<Creature>() })
        {
            if (creature->IsFrozen())
                creature->EndStun();
            else
                creature->Perish();

            Dose();
        }
        else if (age_ > 1.1f || otherNode->GetName() == "Liquid")
        {
            Dose();
        }
    }
    else return;

    /// Too late: Needs customized callback
//    RigidBody* otherBody{ static_cast<RigidBody*>(eventData[NodeCollisionStart::P_OTHERBODY].GetPtr()) };
//    if (otherBody)
//        rigidBody_->SetRestitution(1.f / Max(0.1f, otherBody->GetRestitution()));
}

void Fireball::Dose()
{
    age_ = 1.7f;
    velocity_ = 0.f;
}

void Fireball::HandleNodeCollisionEnd(StringHash eventType, VariantMap& eventData)
{
    rigidBody_->SetLinearVelocity(rigidBody_->GetLinearVelocity().Normalized() * velocity_);
}
