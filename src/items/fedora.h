/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FEDORA_H
#define FEDORA_H

#include "item.h"

class WhipGrip;

class Fedora: public Item
{
    DRY_OBJECT(Fedora, Item);

public:
    static void RegisterObject(Context* context);
    Fedora(Context* context);

protected:
    void OnNodeSet(Node* node) override;
};

#define WHIP_REACH 15.f
#define WHIP_RADIUS .23f

class Bullwhip: public SceneObject
{
    DRY_OBJECT(Bullwhip, SceneObject);

public:
    Bullwhip(Context* context);
    void PostUpdate(float timeStep) override;
    void FixedUpdate(float timeStep) override;

    Ray GetWhipRay(float radius = WHIP_RADIUS) const
    {
        const Vector3 dir{ node_->GetWorldDirection() };
        return { node_->GetWorldPosition() - node_->GetWorldUp() * radius - dir, dir };
    }

    void Grip(WhipGrip* whipGrip = nullptr);
    bool IsGripping() const { return gripped_ != nullptr; }

protected:
    void OnNodeSet(Node* node) override;

private:
    void FindTarget();
    bool HitCheck(PhysicsRaycastResult& result);
    void HandlePostRenderUpdate(const StringHash eventType, VariantMap& eventData);

    AnimatedModel* grip_;
    AnimatedModel* curl_;
    AnimatedModel* tail_;
    Node* target_;
    WhipGrip* gripped_;
    float stretch_;
    bool expired_;
};

#endif // FEDORA_H
