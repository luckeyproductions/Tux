/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../blockmap/block.h"

#include "hardhat.h"

PODVector<Node*> HardHat::brickAndMortarNodes_{};
HashSet<IntVector3> HardHat::bricks_{};
HashSet<IntVector3> HardHat::foundation_{};
int HardHat::ground_{};
IntVector3 HardHat::allowedCoords_{ M_MAX_INT * IntVector3::ONE };

HardHat::HardHat(Context* context): Item(context)
{
}

void HardHat::OnNodeSet(Node* node)
{
    if (!node_)
        return;

    Item::OnNodeSet(node);
    SetHat(GetTypeName());
}

bool HardHat::AllowBrick(Scene* scene, IntVector3& coords)
{
    int y{ M_MAX_INT };
    for (int dy{ 1 }; dy >= -1; --dy )
    {
        const IntVector3 shifted{ coords + IntVector3::UP * dy };
        if (shifted.y_ <= ground_)
            break;
        if (bricks_.Contains(shifted) || foundation_.Contains(shifted))
            continue;

        const BoundingBox checkBounds{ shifted - Vector3::ONE * .425f, shifted + Vector3::ONE * .425f };
        PhysicsWorld* physics{ scene->GetComponent<PhysicsWorld>() };
        PODVector<RigidBody*> bodies{};

        if (physics->GetRigidBodies(bodies, checkBounds, LAYER(L_STATIC) | LAYER(L_ENEMY)))
            continue;

        for (int d{ 0 }; d <= 4; ++d)
        {
            IntVector3 direction{ IntVector3::DOWN };
            switch (d)
            {
            default: break;
            case 1: direction = IntVector3::RIGHT;   break;
            case 2: direction = IntVector3::LEFT;    break;
            case 3: direction = IntVector3::FORWARD; break;
            case 4: direction = IntVector3::BACK;    break;
            }

            if (SolidCheck(shifted, direction))
                y = dy;
        }

        if (y < M_MAX_INT)
        {
            coords += y * IntVector3::UP;
            allowedCoords_ = coords;

            return true;
        }
    }

    allowedCoords_ = IntVector3::ONE * M_MAX_INT;
    return false;
}

bool HardHat::SolidCheck(const IntVector3& coords, const IntVector3& direction)
{
    const IntVector3 shifted{ coords + direction };

    if (bricks_.Contains(shifted) ||
        (direction.y_ < 0 && IsBearing(coords)))
        return true;

    return false;
}

bool HardHat::IsBearing(const IntVector3& coords)
{
    const IntVector3 below{ coords + IntVector3::DOWN };
    return (below.y_ == ground_ || foundation_.Contains(below));
}

bool HardHat::LayBrick(Scene* scene, const Vector3& position)
{
    if (!scene)
        return false;

    IntVector3 coords{ VectorRoundToInt(position) };
    if (!AllowBrick(scene, coords))
        return false;

    Node* blockNode{ scene->CreateChild("Bricks") };
    blockNode->SetPosition(coords);
    blockNode->SetRotation({ 90.f * ((coords.x_ + coords.y_ + coords.z_) % 2) + RandomBool() * 180.f, Vector3::UP });

    StaticModel* block{ blockNode->CreateComponent<StaticModel>() };
    block->SetModel(scene->RES(Model, "Models/Bricks.mdl"));
    block->SetMaterial(scene->RES(Material, "Materials/VCol.xml"));
    block->SetCastShadows(true);
    blockNode->CreateComponent<RigidBody>();
    CollisionShape* collider{ blockNode->CreateComponent<CollisionShape>() };
    collider->SetBox(Vector3::ONE);
    collider->SetMargin(.025f);

    brickAndMortarNodes_.Push(blockNode);
    bricks_.Insert(coords);

    AddMortar(scene, coords);

    return true;
}

void HardHat::AddMortar(Scene* scene, const IntVector3& coords)
{
    for (const IntVector3& direction: AllDirections())
    {
        const IntVector3 shifted{ coords + direction };
        if (bricks_.Contains(shifted))
        {
            Node* mortarNode{ scene->CreateChild("Mortar") };
            mortarNode->SetPosition(coords + .5f * direction);
            mortarNode->LookAt(coords + direction, (direction.y_ == 0 ? Vector3::UP : Vector3::RIGHT));
            mortarNode->Rotate({ Random(4) * 90.f, Vector3::FORWARD });

            StaticModel* mortar{ mortarNode->CreateComponent<StaticModel>() };
            mortar->SetModel(scene->RES(Model, "Models/Mortar.mdl"));
            mortar->SetMaterial(scene->RES(Material, "Materials/VCol.xml"));

            brickAndMortarNodes_.Push(mortarNode);
        }
    }
}

bool HardHat::AllowChisel(IntVector3& coords)
{
    for (int dy{ 1 }; dy >= -1; --dy )
    {
        const IntVector3 shifted{ coords + IntVector3::UP * dy };
        if (bricks_.Contains(shifted) && !IsEssential(shifted))
        {
            coords = shifted;
            allowedCoords_ = coords;

            return true;
        }
    }

    allowedCoords_ = IntVector3::ONE * M_MAX_INT;
    return false;
}

bool HardHat::Chisel(const Vector3& position)
{
    IntVector3 coords{ VectorRoundToInt(position) };

    if (AllowChisel(coords))
    {
        bricks_.Erase(coords);

        PODVector<Node*> remainingNodes;
        for (Node* n: brickAndMortarNodes_)
        {
            if (n->GetWorldPosition().DistanceToPoint(coords) < M_1_SQRT2)
                n->Remove();
            else
                remainingNodes.Push(n);
        }
        brickAndMortarNodes_ = remainingNodes;

        return true;
    }

    return false;
}

bool HardHat::IsEssential(const IntVector3& coords)
{
    for (const IntVector3& direction: AllDirections())
    {
        HashSet<IntVector3> area{};
        Flood(coords + direction, coords, area);

        bool supported{ area.IsEmpty() };
        for (const IntVector3& brick: area)
        {
            if (IsBearing(brick))
            {
                supported = true;
                break;
            }
        }

        if (!supported)
            return true;
    }

    return false;
}

void HardHat::Flood(const IntVector3& coords, const IntVector3& ignore, HashSet<IntVector3>& area)
{
    if (!bricks_.Contains(coords) || area.Contains(coords) || coords == ignore)
        return;

    area.Insert(coords);

    for (const IntVector3& direction: AllDirections())
        Flood(coords + direction, ignore, area);
}

