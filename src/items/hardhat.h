/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HARDHAT_H
#define HARDHAT_H

#include "item.h"

class HardHat: public Item
{
    DRY_OBJECT(HardHat, Item);

public:
    static void Clear()
    {
        for (Node* n: brickAndMortarNodes_)
            n->Remove();

        brickAndMortarNodes_.Clear();
        bricks_.Clear();
        foundation_.Clear();
        ground_ = 0;
    }

    static bool AllowBrick(Scene* scene, IntVector3& coords);
    static bool LayBrick(Scene* scene, const Vector3& position);
    static IntVector3 GetAllowedCoords() { return allowedCoords_; }

    static bool AllowChisel(IntVector3& coords);
    static bool Chisel(const Vector3& position);

    static void AddFoundation(const IntVector3& coords)
    {
        foundation_.Insert(coords);
    }

    static void SetGround(int y)
    {
        ground_ = y;
    }

    HardHat(Context* context);

protected:
    void OnNodeSet(Node* node) override;

private:
    static bool SolidCheck(const IntVector3& coords, const IntVector3& direction);
    static bool NeighbourCheck(const IntVector3& coords, const Vector3& direction);
    static void AddMortar(Scene* scene, const IntVector3& coords);

    static bool IsBearing(const IntVector3& coords);
    static bool IsEssential(const IntVector3& coords);
    static void Flood(const IntVector3& coords, const IntVector3& ignore, HashSet<IntVector3>& area);

    static PODVector<IntVector3> AllDirections()
    {
        return { IntVector3::RIGHT,   IntVector3::LEFT,
                 IntVector3::UP,      IntVector3::DOWN,
                 IntVector3::FORWARD, IntVector3::BACK };
    }

    static PODVector<Node*> brickAndMortarNodes_;
    static HashSet<IntVector3> bricks_;
    static HashSet<IntVector3> foundation_;
    static int ground_;
    static IntVector3 allowedCoords_;
};

#endif // HARDHAT_H
