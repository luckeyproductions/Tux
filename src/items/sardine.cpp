/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../ui/dash.h"
#include "../spawnmaster.h"
#include "../jukebox.h"

#include "sardine.h"

Sardine::Sardine(Context* context): Item(context)
{
    pickupSound_ = "Splish.wav";
    pickupSoundGain_ = .9f;
}

void Sardine::OnNodeSet(Node* node)
{
    if (!node_)
        return;

    Item::OnNodeSet(node);

    graphicsNode_->Pitch(-55.f);

    model_->SetModel(RES(Model, "Models/Sardine.mdl"));
    model_->SetMaterial(RES(Material, "Materials/VColOutlineMetallic.xml"));
}

void Sardine::PickUp()
{
    Item::PickUp();

    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>()};
    const unsigned numSardines{ spawn->CountActive<Sardine>() };
    GetSubsystem<Dash>()->SetSardineCount(numSardines);

    if (numSardines == 0u)
    {
        SendEvent(E_LASTSARDINE);

        PlaySample("Win.wav", 0.f, .9f, false);
        GetSubsystem<JukeBox>()->Play("");
    }
}
