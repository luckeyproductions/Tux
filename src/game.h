/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GAME_H
#define GAME_H

#include "tux.h"

#include "mastercontrol.h"

enum GameStatus{ GS_MAIN, GS_PLAY, GS_PAUSED, GS_MODAL };
enum GameMode{ GM_STORY, GM_RUN, GM_BATTLE };

class Player;
class BlockMap;
class TuxCam;

typedef struct World
{
    Scene* scene_{ nullptr };
    BlockMap* level_{ nullptr };

    Tux* tux_{ nullptr };
    TuxCam* camera_{ nullptr };

    struct {
        SharedPtr<Node> sceneCursor{ nullptr };
        SharedPtr<Cursor> uiCursor{ nullptr };
        PODVector<RayQueryResult> hitResults{};
    } cursor_;
} World;


class Game: public Object
{
    DRY_OBJECT(Game, Object);

public:
    static void RegisterObject(Context* context);

    Game(Context* context);

    GameStatus GetStatus() const { return status_; }
    void SetStatus(GameStatus status);

    void StartNew(GameMode mode = GM_RUN, const String& map = "");
    void TogglePause();
    void UnpauseScene();

    void CreateMenuScene();
    void LoadMap(const String& mapName);

    Scene* GetScene() const { return world_.scene_; }
    World& GetWorld() { return world_; }

    Vector< SharedPtr<Player> > GetPlayers() const;
    Player* GetPlayer(int playerID) const;
    Player* GetNearestPlayer(const Vector3& pos);

    void SetFadeMultiplier(float fadeMultiplier);
    void Fade(float timeStep);

private:
    Scene* CreateScene(bool mainMenu = false);

    Scene* menuScene_;
    World world_;
    GameStatus status_;
    GameMode mode_;
    Vector< SharedPtr<Player> > players_;
};

DRY_EVENT(E_GAMESTATUSCHANGED, GameStatusChanged)
{
    DRY_PARAM(P_OLDSTATUS, OldStatus); // int
    DRY_PARAM(P_STATUS, Status);       // int
}

#endif // GAME_H
