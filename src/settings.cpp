/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "settings.h"

GraphicsSettings::GraphicsSettings(Context* context): Serializable(context),
  fullScreen_{ true },
  vSync_{ true },
  antiAliasing_{ true },
  bloom_{ true },
  ambientOcclusion_{ true },
  resolution_{}
{
}

void GraphicsSettings::RegisterObject(Context* context)
{
    context->RegisterFactory<Settings>();

    DRY_ATTRIBUTE(GS_ANTIALIASING, bool, antiAliasing_,     true, AM_FILE);
    DRY_ATTRIBUTE(GS_BLOOM,        bool, bloom_,            true, AM_FILE);
    DRY_ATTRIBUTE(GS_SSAO,         bool, ambientOcclusion_, true, AM_FILE);
    DRY_ATTRIBUTE(GS_VSYNC,        bool, vSync_,            true, AM_FILE);
    DRY_ATTRIBUTE(GS_FULLSCREEN,   bool, fullScreen_,       true, AM_FILE);
    DRY_ATTRIBUTE(GS_RESOLUTION, IntVector2, resolution_, IntVector2::ZERO, AM_FILE);
}

AudioSettings::AudioSettings(Context* context): Serializable(context),
    musicEnabled_{ true },
    musicGain_{ .42f },
    effectsEnabled_{ true },
    effectsGain_{ 1.f }
{
}

void AudioSettings::RegisterObject(Context* context)
{
    context->RegisterFactory<Settings>();

    DRY_ATTRIBUTE(AS_MUSIC_ENABLED,    bool, musicEnabled_,   true, AM_FILE);
    DRY_ATTRIBUTE(AS_MUSIC_GAIN,      float, musicGain_,      .42f, AM_FILE);
    DRY_ATTRIBUTE(AS_SAMPLES_ENABLED,  bool, effectsEnabled_, true, AM_FILE);
    DRY_ATTRIBUTE(AS_SAMPLES_GAIN,    float, effectsGain_,     1.f, AM_FILE);
}

void Settings::RegisterObject(Context* context)
{
    context->RegisterFactory<Settings>();

    GraphicsSettings::RegisterObject(context);
    AudioSettings::RegisterObject(context);
}

Settings::Settings(Context* context): Object(context),
    graphics_{ new GraphicsSettings(context) },
    audio_{ new AudioSettings(context) }
{
}

bool Settings::LoadXML(const XMLElement& source)
{
    if (!graphics_->LoadXML(source.GetChild("graphics")))
        return false;
    if (!audio_->LoadXML(source.GetChild("audio")))
        return false;

    return true;
}

bool Settings::SaveXML(XMLElement& dest)
{
    XMLElement graphicsElement{ dest.CreateChild("graphics") };
    if (!graphics_->SaveXML(graphicsElement))
        return false;

    XMLElement audioElement{ dest.CreateChild("audio") };
    if (!audio_->SaveXML(audioElement))
        return false;

    return true;
}
