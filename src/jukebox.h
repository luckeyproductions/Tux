/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef JUKEBOX_H
#define JUKEBOX_H

#include "mastercontrol.h"

struct Song {
    String name_;
    float gain_;
};

class JukeBox: public Object
{
    const Vector<Song> songs_
    {
        { "Woke Up This Morning.ogg", 1.f },
        { "Music Box Rag.ogg", .5f },
        { "Rocky Top.ogg", .5f },
        { "Com Mecha.ogg", .75f }
    };

    DRY_OBJECT(JukeBox, Object);

public:
    enum Songs{ MUSIC_MENU, MUSIC_STORY, MUSIC_RUN };
    JukeBox(Context* context);

    Node* GetMusicNode() const { return musicNode_; }

    void Play(int s)
    {
        Song song{ songs_.At(s) };
        Play(song.name_, true, song.gain_);
    }

    void Play(const String& song)
    {
        float gain{ 1.f };

        for (const Song& s: songs_)
            if (song == s.name_)
                gain = s.gain_;

        Play(song, true, gain);
    }

    void Play(const String& song, bool loop, float gain = 1.f);

private:
    void HandleMusicFaded(StringHash eventType, VariantMap& eventData);

    Node* musicNode_;
    Vector<SoundSource*> tracks_;
};

DRY_EVENT(E_MUSICFADED, MusicFaded)
{
}

#endif // JUKEBOX_H
