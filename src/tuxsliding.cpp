/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ui/oxygenindicator.h"
#include "blockmap/blockmap.h"
#include "tux.h"

#include "tuxsliding.h"

TuxSliding::TuxSliding(Context* context): LogicComponent(context),
    tux_{ nullptr },
    move_{},
    rigidBody_{ nullptr },
    collisionShape_{ nullptr },
    smoothVelocity_{},
    sinceDash_{ INTERVAL_DASH }
{
}

void TuxSliding::OnNodeSet(Node* node)
{
    if (!node)
        return;

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(2.3f);
    rigidBody_->SetLinearRestThreshold(.05f);
    rigidBody_->SetAngularFactor(Vector3::UP);
    rigidBody_->SetRestitution(1.f);
    rigidBody_->SetLinearDamping(.1f);
    rigidBody_->SetAngularDamping(.5f);
    rigidBody_->SetRollingFriction(.1f);
    rigidBody_->SetCollisionLayer(LAYER(L_PLAYER));
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED - LAYER(L_PLAYER) - LAYER(L_PROJECTILE));
    collisionShape_ = node_->CreateComponent<CollisionShape>();
    collisionShape_->SetSphere(4/5.f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(TuxSliding, HandleNodeCollisionStart));
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(TuxSliding, HandlePostRenderUpdate));
}

void TuxSliding::Launch()
{
    rigidBody_->ApplyImpulse(node_->GetWorldDirection() * 5.f);
}

void TuxSliding::Dash()
{
    if (sinceDash_ < INTERVAL_DASH || tux_->IsStunned())
        return;

    Node* graphicsNode{ node_->GetChild("Graphics") };
    AnimationController* animCtrl{ graphicsNode->GetComponent<AnimationController>() };
    animCtrl->StopLayer(1, .11f);

    sinceDash_ = 0.f;
    const Quaternion camYawRot{ GetScene()->GetChild("TuxCam")->GetWorldRotation().YawAngle(), Vector3::UP };
    Vector3 move{ camYawRot * move_ };
    const Vector3 direction{ graphicsNode->GetWorldDirection().Lerp(move, .8f).NormalizedOrDefault(move) };
    rigidBody_->ApplyImpulse(direction * 17.f);
}

void TuxSliding::FixedUpdate(float timeStep)
{
    Node* graphicsNode{ node_->GetChild("Graphics") };

    const Vector3 lv{ rigidBody_->GetLinearVelocity() };

    if (!IsUnderwater())
    {
        const Vector3 right{ graphicsNode->GetWorldRight().ProjectOntoPlane(SurfaceNormal()).Normalized() };

        const float newFriction{ .17f - .13f * Max(-.23f, move_.z_ - Abs(move_.x_)) };
        rigidBody_->SetFriction(Lerp(rigidBody_->GetFriction(), newFriction, Clamp(timeStep * 5.f, 0.f, 1.f)));
        rigidBody_->ApplyForce(right * move_.x_ * timeStep * 420.f * Min(7.f, PowN(Max(0.f, graphicsNode->GetWorldDirection().DotProduct(lv) - .1f) * .23f, 2)));
        rigidBody_->ApplyTorque(Vector3::DOWN * timeStep * rigidBody_->GetAngularVelocity().y_ * rigidBody_->GetMass() * 230.f);
    }
    else if (!tux_->IsStunned())
    {
        const Quaternion camYawRot{ GetScene()->GetChild("TuxCam")->GetWorldRotation().YawAngle(), Vector3::UP };
        Vector3 move{ camYawRot * move_ };
        if (move.y_ < 0.f)
            move.y_ *= .5f;

        rigidBody_->ApplyImpulse(timeStep * move * (23.f - rigidBody_->GetLinearVelocity().DotProduct(move)));
    }
}

void TuxSliding::FixedPostUpdate(float timeStep)
{
    Node* graphicsNode{ node_->GetChild("Graphics") };

    const Vector3 forward{ graphicsNode->GetWorldDirection() };
    const Vector3 up{ graphicsNode->GetWorldUp() };
    const Vector3 lv{ rigidBody_->GetLinearVelocity() };

    // Head bumping
    PhysicsWorld* physicsWorld{ GetScene()->GetComponent<PhysicsWorld>() };
    const Vector3 rayDirection{ forward };
    const Vector3 rayOrigin{ node_->GetWorldPosition() + graphicsNode->GetWorldRotation() * collisionShape_->GetPosition() };
    const Ray headRay{ rayOrigin, rayDirection };
    PhysicsRaycastResult result{};
    float dist{ 1.f };
    physicsWorld->RaycastSingle(result, headRay, dist, M_MAX_UNSIGNED - LAYER(L_PLAYER) - LAYER(L_PROJECTILE));
    if (result.body_ && !result.body_->IsTrigger())
    {
        const float frac{ (1.f - result.hitFraction_) };
        const float direct{ result.normal_.DotProduct(lv.Normalized()) };
        if (!tux_->IsStunned(true) && direct > M_1_SQRT2)
        {
            const bool stun{ lv.Length() > (1.3f + IsUnderwater() * 5.5f) };
            rigidBody_->SetLinearVelocity(result.normal_ * (2.f * stun + 3.f));

            VariantMap eventData{};
            eventData[HardBump::P_STUN] = stun;
            SendEvent(E_HARDBUMP, eventData);
        }
        else
        {
            Quaternion bump{};
            bump.FromRotationTo(forward, result.normal_.ProjectOntoPlane(up).Normalized());
            const Vector3 euler{ bump.EulerAngles() };
            rigidBody_->ApplyTorque(23.f * euler * Max(0.f, frac - .23f) * timeStep);
            rigidBody_->ApplyForce(-forward * Cbrt(direct) * Max(lv.Length(), Sqrt(frac) * 100.f) * rigidBody_->GetMass() * timeStep);
        }
    }
}

void TuxSliding::Update(float timeStep)
{
    sinceDash_ += timeStep;

    Node* graphicsNode{ node_->GetChild("Graphics") };
    const bool submerged{ IsUnderwater() };
    const float toSurface{ GetSubsystem<BlockMap>()->ToSurface(node_->GetWorldPosition()) / (.5f * collisionShape_->GetSize().y_) };
    float under{ (!submerged ? 0.f : Sqrt(Min(1.f, toSurface))) };
    OxygenIndicator* oxygen{ GetSubsystem<OxygenIndicator>() };
    if (under < 1.f)
        oxygen->FillOxygen();
    oxygen->SetShowOxygen(GetSubsystem<BlockMap>()->HasWater() && toSurface > -.5f);

    const Vector3 worldGravity{ GetScene()->GetComponent<PhysicsWorld>()->GetGravity() };
    rigidBody_->SetGravityOverride(worldGravity * (1.f - Max(0.f, toSurface * submerged)));
    rigidBody_->SetUseGravity(under < 1.f);
    rigidBody_->SetLinearDamping(.1f + .65f * submerged * under);
    AnimationController* animCtrl{ graphicsNode->GetComponent<AnimationController>() };

    const Vector3 lv{ rigidBody_->GetLinearVelocity() };
    const Vector3 groundNormal{ SurfaceNormal().Lerp(Vector3::UP, under) };
    smoothVelocity_ = smoothVelocity_.Lerp(lv, Min(1.f, timeStep * 23.f));
    const float v{ smoothVelocity_.Length() };
    const Vector3 d{ smoothVelocity_.NormalizedOrDefault(graphicsNode->GetWorldDirection().ProjectOntoPlane(groundNormal).Normalized()) };
    const Vector3 front{ graphicsNode->GetWorldPosition() + graphicsNode->GetWorldDirection().Lerp(d, Min(1.f, timeStep * 7.f  * Clamp(PowN(v, 3), 1.f, .23f))) };

    if (!tux_->IsStunned())
    {
        const float sideMove{ (!IsUnderwater() ? move_.x_
                                               : (Quaternion{ GetScene()->GetChild("TuxCam")->GetWorldRotation().YawAngle(), Vector3::UP } * move_).DotProduct(graphicsNode->GetWorldRight())) };
        const Vector3 sway{ graphicsNode->GetWorldRight() * sideMove };
        const Vector3 up{ (groundNormal * 5.f + Vector3::UP + sway).Normalized() };
        graphicsNode->LookAt(front, graphicsNode->GetWorldUp().Lerp(up, Min(1.f, timeStep * 10.f)));

        if (sinceDash_ > .7f)
        {
            animCtrl->PlayExclusive("Animations/Swim.ani", 1, true, .23f);
            animCtrl->SetStartBone("Animations/Swim.ani", "Hips");
            animCtrl->SetWeight("Animations/Swim.ani", Lerp(animCtrl->GetWeight("Animations/Swim.ani"), under, 5.5f * timeStep));
            animCtrl->SetSpeed("Animations/Swim.ani", .2f + (.3f * lv.Length() - .025f * lv.LengthSquared()));
        }
    }
    else
    {
        animCtrl->PlayExclusive("Animations/StunnedUnderwater.ani", 1, true, .23f);
        animCtrl->SetStartBone("Animations/StunnedUnderwater.ani", "Hips");
    }
}

Vector3 TuxSliding::SurfaceNormal()
{
    PhysicsWorld* physicsWorld{ GetScene()->GetComponent<PhysicsWorld>() };
    PhysicsRaycastResult result{};
    physicsWorld->RaycastSingle(result, { node_->GetWorldPosition(), Vector3::DOWN }, .5f, LAYER(L_STATIC));

    return result.distance_ != M_INFINITY ? result.normal_ : Vector3::UP;
}

void TuxSliding::HandlePostRenderUpdate(const StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
//    collisionShape_->DrawDebugGeometry(debug, true);
//    GetScene()->GetComponent<PhysicsWorld>()->DrawDebugGeometry(debug, false);

//    Node* graphicsNode{ node_->GetChild("Graphics") };

//    const Vector3 rayDirection{ graphicsNode->GetWorldDirection() };
//    const Vector3 rayOrigin{ node_->GetWorldPosition() + graphicsNode->GetWorldRotation() * collisionShape_->GetPosition() };
//    Ray headRay{ rayOrigin, rayDirection };
//    debug->AddLine(headRay.origin_, headRay.origin_ + headRay.direction_, Color::RED, false);

//    const IntVector3& mapSize{ GetSubsystem<BlockMap>()->GetMapSize() };
//    if (path_.IsEmpty())
//    {
//        PathFinder* pf{ GetSubsystem<PathFinder>() };
//        const IntVector3 randomDestination{ Random(mapSize.x_), Random(mapSize.y_), Random(mapSize.z_) };
//        path_ = pf->FindPathOrthogonal(node_->GetWorldPosition(), randomDestination, false);
//    }

//    for (unsigned i{ 0 }; i < path_.Size() - 1; ++i)
//    {
//        const Vector3 from{ path_.At(i) - (mapSize - VectorMod(mapSize, { 2, 2, 2 })) * .5f };
//        const Vector3 to{ path_.At(i + 1) - (mapSize - VectorMod(mapSize, { 2, 2, 2 })) * .5f };
//        debug->AddLine(from, to, Color::WHITE);
//    }

//    if (Random(100) == 0)
//        path_.Clear();
}

bool TuxSliding::IsUnderwater() const
{
    return GetSubsystem<BlockMap>()->IsUnderwater(node_->GetWorldPosition());
}

void TuxSliding::HandleNodeCollisionStart(const StringHash /*eventType*/, VariantMap& eventData)
{
    using namespace NodeCollision;
    Node* otherNode{ static_cast<Node*>(eventData[P_OTHERNODE].GetPtr()) };

    if (otherNode->HasTag("Lava"))
    {
        VariantMap bumpData{};
        bumpData[HardBump::P_BURN] = true;
        SendEvent(E_HARDBUMP, bumpData);
    }
}
