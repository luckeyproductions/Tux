/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "player.h"

#include "mastercontrol.h"
#include "inputmaster.h"
#include "ui/inventory.h"

Player::Player(int playerId, Context* context): Object(context),
    playerId_{ playerId },
    autoPilot_{ false },
    alive_{ true },
    step_{ 0.f },
    score_{ 0u },
    multiplier_{ 1 }
{
}

void Player::Die()
{
    alive_ = false;
}

void Player::Respawn()
{
    ResetScore();
    multiplier_ = 1;
    alive_ = true;
}

void Player::SetScore(int points)
{
    score_ = points;
}

void Player::ResetScore()
{
    SetScore(0);
}

void Player::Step(const int step)
{
    if (step == 0)
    {
        step_ = 0.f;
        return;
    }

    if (!Equals(step_, 0.f))
        step_ = Sign(step_) * Max(0.f, Abs(step_) - GetSubsystem<Time>()->GetTimeStep());

    if (Equals(step_, 0.f) || Sign(step_) != Sign(step))
    {
        GetSubsystem<Inventory>()->Step(step);
        step_ = Sign(step) * .55f;
    }
}

void Player::AddScore(int points)
{
    score_ += points;
}

Controllable* Player::GetControllable()
{
    Controllable* controllable{ GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_) };
    if (controllable)
        return controllable;
    else
        return nullptr;
}
