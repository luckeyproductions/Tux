/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INPUTMASTER_H
#define INPUTMASTER_H

#include "mastercontrol.h"
#include "controllable.h"

enum MasterInputAction { MIA_NONE = 0, MIA_UP, MIA_DOWN, MIA_LEFT, MIA_RIGHT, MIA_INCREMENT, MIA_DECREMENT, MIA_CONFIRM, MIA_CANCEL, MIA_PAUSE, MIA_MENU, MIA_SCREENSHOT };
enum PlayerInputAction { PIA_RUN = 0, PIA_JUMP, PIA_SLIDE, PIA_USE, PIA_PREV, PIA_NEXT,
                         PIA_MOVE_UP, PIA_MOVE_DOWN, PIA_MOVE_LEFT, PIA_MOVE_RIGHT,
                         PIA_AIM_N, PIA_AIM_NE, PIA_AIM_E, PIA_AIM_SE,
                         PIA_AIM_S, PIA_AIM_SW, PIA_AIM_W, PIA_AIM_NW };

struct InputActions
{
    PODVector<MasterInputAction> master_{};
    HashMap<int, PODVector<PlayerInputAction>> player_{};
};

class Player;

class InputMaster: public Object
{
    DRY_OBJECT(InputMaster, Object);

public:
    InputMaster(Context* context);
    void SetPlayerControl(Player* player, Controllable* controllable);
    Player* GetPlayerByControllable(Controllable* controllable);
    Controllable* GetControllableByPlayer(int playerId);
    Vector<Controllable*> GetControllables() { return controlledByPlayer_.Values(); }

private:
    HashMap<int, MasterInputAction> keyBindingsMaster_;
    HashMap<int, MasterInputAction> buttonBindingsMaster_;
    HashMap<int, float> sinceMasterAction_;
    HashMap<int, HashMap<int, PlayerInputAction> > keyBindingsPlayer_;
    HashMap<int, HashMap<int, PlayerInputAction> > buttonBindingsPlayer_;
    HashMap<int, HashMap<int, PlayerInputAction> > mouseBindingsPlayer_;

    Vector<int> pressedKeys_;
    HashMap<int, Vector<ControllerButton> > pressedJoystickButtons_;
    HashMap<int, HashMap<int, float> > axesPosition_;
    HashMap<int, Vector<MouseButton> > pressedMouseButtons_;

    HashMap<int, Controllable*> controlledByPlayer_;

    const float masterInterval_{ .23f };
    bool repeatedConfirm_{ false };
    bool repeatedCancel_{ false };

    void HandleGameStatusChanged(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleKeyUp(StringHash eventType, VariantMap& eventData);
    void HandleJoystickButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleJoystickButtonUp(StringHash eventType, VariantMap& eventData);
    void HandleJoystickAxisMove(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonUp(StringHash eventType, VariantMap& eventData);

    void HandleActions(const InputActions& actions);
    void HandlePlayerAction(PlayerInputAction action, int playerId);
    Vector3 GetMoveFromActions(const PODVector<PlayerInputAction>& actions);
    Vector3 GetAimFromActions(const PODVector<PlayerInputAction>& actions);

    void Screenshot();
    void PauseButtonPressed();
};

DRY_EVENT(E_MENUINPUT, MenuInput)
{
    DRY_PARAM(P_ACTIONS, Actions); // Buffer
}

#endif // INPUTMASTER_H
