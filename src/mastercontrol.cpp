/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "settings.h"
#include "effectmaster.h"
#include "inputmaster.h"
#include "spawnmaster.h"
#include "ui/gui.h"
#include "game.h"
#include "blockmap/cyberplasm/cyberplasm.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    storagePath_{}
{
    Settings::RegisterObject(context_);
}

void MasterControl::Setup()
{
    context_->RegisterSubsystem(this);
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "tux.log";
    engineParameters_[EP_WINDOW_TITLE] = "Tux!";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WINDOW_RESIZABLE] = true;

    //Add resource path
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    String resourcePath{ "Resources" };
    if (!fs->DirExists(AddTrailingSlash(fs->GetProgramDir()) + resourcePath))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (fs->DirExists(installedResources))
            resourcePath = installedResources;
    }

    if (resourcePath == "Resources")
        storagePath_ = resourcePath;
    else
        storagePath_ = RemoveTrailingSlash(fs->GetAppPreferencesDir("luckey", "tux"));

    engineParameters_[EP_RESOURCE_PATHS] = resourcePath;

    //Load settings
    Settings* settings{ context_->RegisterSubsystem<Settings>() };
    XMLFile* settingsFile{ RES_NO(XMLFile, SETTINGSPATH) };

    if (settingsFile && settings->LoadXML(settingsFile->GetRoot()))
    {
        GraphicsSettings* graphicsSettings{ settings->GetGraphicsSettings() };
        const IntVector2 resolution{ graphicsSettings->GetAttribute("Resolution").GetIntVector2() };

        engineParameters_[EP_WINDOW_WIDTH]  = resolution.x_;
        engineParameters_[EP_WINDOW_HEIGHT] = resolution.y_;
        engineParameters_[EP_FULL_SCREEN] = graphicsSettings->GetAttribute(GS_FULLSCREEN).GetBool();
        engineParameters_[EP_VSYNC] = graphicsSettings->GetAttribute(GS_VSYNC).GetBool();
    }
}

void MasterControl::Start()
{
    Graphics* graphics{ GetSubsystem<Graphics>() };
    if (graphics)
        GetSubsystem<Engine>()->SetMaxFps(graphics->GetRefreshRate());

    SetupRenderer();

    context_->RegisterSubsystem<EffectMaster>();
    context_->RegisterSubsystem<InputMaster>();
    context_->RegisterSubsystem<SpawnMaster>();
    context_->RegisterSubsystem<Cyberplasm>();

    Game::RegisterObject(context_);
    Game* game{ context_->RegisterSubsystem<Game>() };
    context_->RegisterSubsystem<GUI>();

    const String argMap{ HandleArguments() };
    game->CreateMenuScene();
    if (!argMap.IsEmpty())
        game->StartNew(GM_RUN, argMap);
    else
        game->SetStatus(GS_MAIN);
}

String MasterControl::HandleArguments()
{
    for (const String& arg: GetArguments())
    {
        Log::Write(LOG_INFO, arg);
        String extraDir{ arg };
        String mapName{ arg };

        if (arg.EndsWith(".emp", false))
        {
            const unsigned mPos{ arg.Find("Maps") };
            if (mPos > 0 && mPos != String::NPOS)
            {
                extraDir = arg.Substring(0, mPos - 1);
                mapName  = arg.Substring(extraDir.Length());
            }
            else
            {
                extraDir = "";
            }
        }
        else
        {
            mapName = "";
        }

        if (!extraDir.IsEmpty())
        {
            ResourceCache* cache{ GetSubsystem<ResourceCache>() };
            cache->AddResourceDir(extraDir, 0);
        }

        if (!mapName.IsEmpty() && RES(XMLFile, mapName))
            return mapName;
    }

    return "";
}

void MasterControl::SetupRenderer()
{
    Renderer* renderer{ GetSubsystem<Renderer>() };
    renderer->SetShadowMapSize(2048);
    renderer->SetShadowQuality(SHADOWQUALITY_PCF_24BIT);

    Zone* zone{ renderer->GetDefaultZone() };
    zone->SetAmbientColor(Color{ .4f, .42f, .46f });
    zone->SetFogColor(DEFAULT_FOG_COLOR);
    zone->SetFogEnd(55.f);
    zone->SetFogStart(23.f);

    GraphicsSettings* graphicsSettings{ GetSubsystem<Settings>()->GetGraphicsSettings() };

    RenderPath* renderPath{ renderer->GetDefaultRenderPath() };
    renderPath->Load(RES(XMLFile, "RenderPaths/ForwardDepth.xml"));
    renderPath->Append(RES(XMLFile, "PostProcess/SSAO.xml"));
    renderPath->SetEnabled("SSAO", graphicsSettings->GetAttribute(GS_SSAO).GetBool());
    renderPath->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
    renderPath->SetEnabled("FXAA3", graphicsSettings->GetAttribute(GS_ANTIALIASING).GetBool());
    renderPath->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    renderPath->SetShaderParameter("BloomHDRThreshold", .55f);
    renderPath->SetShaderParameter("BloomHDRMix", Vector2(.95f, .17f));
    renderPath->SetEnabled("BloomHDR", graphicsSettings->GetAttribute(GS_BLOOM).GetBool());
    renderPath->Append(RES(XMLFile, "PostProcess/Fade.xml"));
    renderPath->SetEnabled("Fade", false);
    renderPath->Append(RES(XMLFile, "PostProcess/Distortion.xml"));
}

void MasterControl::Stop()
{
    SharedPtr<XMLFile> settingsFile{ context_->CreateObject<XMLFile>() };
    Settings* settings{ GetSubsystem<Settings>() };
    XMLElement settingsRoot{ settingsFile->CreateRoot("settings") };
    if (settings->SaveXML(settingsRoot))
        settingsFile->SaveFile(SETTINGSPATH);

//    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}
