/* Power Particles
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "particlesystem.h"

ParticleSystem::ParticleSystem(Context* context): Component(context),
    emitting_{ 1 },
    spawnInterval_{ .075f },
    sinceSpawn_{ spawnInterval_ },
    lifeExpectancy_{ 5.f },
    particleGenerator_{},
    particles_{},
    billboards_{ nullptr },
    emissary_{}
{
}

ParticleSystem::~ParticleSystem() = default;

void ParticleSystem::OnSetEnabled() { Component::OnSetEnabled(); }
void ParticleSystem::OnNodeSet(Node* node)
{
    if (!node)
    {
        UnsubscribeFromAllEvents();
        return;
    }

    SubscribeToEvent(E_UPDATE, DRY_HANDLER(ParticleSystem, HandleUpdate));
    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(ParticleSystem, HandlePostUpdate));
}

void ParticleSystem::OnSceneSet(Scene* scene)
{
    UnsubscribeFromEvent(E_POSTRENDERUPDATE);
    if (!scene)
        return;

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(ParticleSystem, HandlePostRenderUpdate));
}

void ParticleSystem::HandleUpdate(const StringHash /*eventType*/, VariantMap& eventData)
{
    const float dt{ eventData[Update::P_TIMESTEP].GetFloat() };

    if (sinceSpawn_ >= spawnInterval_ && particles_.Size() < 8e3)
        Trigger();

    sinceSpawn_ += dt;
    for (PowerParticle* p: particles_)
    {
        p->Update(dt);

        if (p->age_ > lifeExpectancy_)
            particles_.Remove(p);
    }
}

void ParticleSystem::Trigger()
{
    for (int p{ 0 }; p < Abs(emitting_); ++p)
    {
        particles_.Push(particleGenerator_.RandomParticle());
        //    particles_.Push(particleGenerator_.LerpedParticle(Cycle(scene_->GetElapsedTime() * .5f, -2.f, 3.f)));
    }

    sinceSpawn_ = 0.f;
}

void ParticleSystem::HandlePostUpdate(const StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!billboards_)
        return;

    billboards_->SetNumBillboards(particles_.Size());

    for (unsigned p{ 0u }; p < particles_.Size(); ++p)
    {
        Billboard* bb{ billboards_->GetBillboard(p) };
        PowerParticle* particle{ particles_.At(p) };

        bb->enabled_ = true;
        bb->position_ = particle->GetPosition();
        bb->rotation_ = bb->position_.DotProduct(Vector3::ONE) * 180.f;
        const Vector3 particleScale{ particle->GetScale() };
        bb->size_ = { particleScale.ProjectOntoPlane(Vector3::UP).Length(),
                      Abs(particleScale.DotProduct(Vector3::UP)) };
        bb->color_ = particle->GetColor();
    }

    billboards_->Commit();
}

void ParticleSystem::HandlePostRenderUpdate(const StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (billboards_ != nullptr || !node_)
        return;

    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
    if (!debug)
        return;

    for (const PowerParticle* p: particles_)
    {
//        col.FromHSV(col.r_, col.g_, col.b_, col.a_);

//        const Vector3 normal{ (scene_->GetChild("Camera")->GetWorldPosition() - pos).Normalized() };

        const Vector3 scale{ p->GetScale() };
        debug->AddCircle(node_->GetTransform() * p->GetPosition(),
                         Quaternion{ p->GetRotation() } * Vector3::UP * scale.y_,
                         Max(scale.x_, scale.z_),
                         p->GetColor(), 8, false);
    }
}
