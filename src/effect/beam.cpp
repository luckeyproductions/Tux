/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "beam.h"
#define V1 Vector3::ONE

Beam::Beam(Context* context): ParticleSystem(context)
{
    const Vector3 minDir{ Vector3::DOWN * .17f };
    const Vector3 maxDir{ Vector3::UP   * .23f };
    const TypedPolynomial<Vector3> minPosPoly{ { minDir * 3/4.f + Vector3{ -.42f, -.25f, -.42f }, minDir * 5.f,  maxDir * 5.f } };
    const TypedPolynomial<Vector3> maxPosPoly{ { maxDir * 3/4.f + Vector3{  .42f,   .5f,  .42f }, maxDir * 23.f, minDir * 5.f } };
    const TypedBipolynomial<Vector3> posBipoly{ minPosPoly, maxPosPoly };
    particleGenerator_.position_ = posBipoly;

    const TypedPolynomial<Vector3> minScalePoly{ { V1 * .1f, V1 * .23f, V1 * -1.f, V1 * -5.f } };
    const TypedPolynomial<Vector3> maxScalePoly{ { V1 * .1f, V1 * .17f, V1 *  .5f, V1 * -5.f } };
    const TypedBipolynomial<Vector3> scaleBipoly{ minScalePoly, maxScalePoly };
    particleGenerator_.scale_ = scaleBipoly;

    particleGenerator_.color_ = TypedBipolynomial<Color>{ TypedPolynomial<Color>{ { Color{ .2f, .62f, .6f, .25f }, Color{ -.05f, -.05f, -.05f, .5f }, Color{ -.1f, -.1f, -.1f, -3.f } } },
                                                          TypedPolynomial<Color>{ { Color{ .7f, .72f, .8f, .75f },  Color{ -.05f, -.05f, -.05f, .3f }, Color{  .1f,  .1f,  .1f, -4.f } } }};
    lifeExpectancy_ = 1/4.f;
    emitting_ = 64;
}

void Beam::OnNodeSet(Node* node)
{
    ParticleSystem::OnNodeSet(node);

    if (!node)
        return;

    billboards_ = node_->CreateComponent<BillboardSet>();
    billboards_->SetMaterial(RES(Material, "Materials/Sparkle.xml"));
}

void Beam::HandleUpdate(const StringHash eventType, VariantMap& eventData)
{
    ParticleSystem::HandleUpdate(eventType, eventData);

    if (particles_.Size() > 128)
        emitting_ = 0;

    if (particles_.IsEmpty() && GetNode()->GetNumChildren() == 0u)
        node_->Remove();
}
