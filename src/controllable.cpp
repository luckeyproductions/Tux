/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "controllable.h"
#include "player.h"

Controllable::Controllable(Context* context): SceneObject(context),
    controlled_{ false },
    move_{},
    aim_{},
    maxPitch_{ 90.f },
    minPitch_{ 0.f },

    actions_{},
    actionSince_{},
    actionInterval_{},

    graphicsNode_{ nullptr },
    model_{ nullptr },
    rigidBody_{ nullptr },
    collisionShape_{ nullptr },
    animCtrl_{ nullptr }
{
    actionSince_[PIA_RUN]   = actionInterval_[PIA_RUN]   = .0f;
    actionSince_[PIA_JUMP]  = actionInterval_[PIA_JUMP]  = .17f;
    actionSince_[PIA_SLIDE] = actionInterval_[PIA_SLIDE] = .23f;
    actionSince_[PIA_USE]  = actionInterval_[PIA_USE]  = .05f;
}

void Controllable::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);

    graphicsNode_ = node_->CreateChild("Graphics");

    model_ = graphicsNode_->CreateComponent<AnimatedModel>();
    model_->SetCastShadows(true);

    animCtrl_ = graphicsNode_->CreateComponent<AnimationController>();

    rigidBody_ = node_->CreateComponent<RigidBody>();
    collisionShape_ = node_->CreateComponent<CollisionShape>();
}

void Controllable::Update(float timeStep)
{
    for (int a{ 0 }; a < static_cast<int>(actions_.size()); ++a)
    {
        if (actions_[a] || actionSince_[a] > 0.f)
            actionSince_[a] += timeStep;
    }

    if (GetPlayer() && GetPlayer()->IsHuman())
    {
    }
    else
    {
        Think(timeStep);
    }
}

void Controllable::SetMove(Vector3 move)
{
    if (move.Length() > 1.f)
        move.Normalize();

    move_ = move;
}

void Controllable::SetAim(Vector3 aim)
{
    aim_ = aim;
}

void Controllable::SetActions(std::bitset<4> actions)
{
    if (actions == actions_)
    {
        return;
    }
    else
    {

        for (int i{ 0 }; i < static_cast<int>(actions.size()); ++i)
        {
            if (actions[i] != actions_[i])
            {
                actions_[i] = actions[i];

                if (!actions[i])
                    EndAction(i);
                if (actions[i] && actionSince_[i] > actionInterval_[i])
                    HandleAction(i);
            }
        }
    }
}

bool Controllable::Bounce() const
{
    return *actionSince_[PIA_JUMP] < .125f;
}

void Controllable::HandleAction(int actionId)
{
    actionSince_[actionId] = M_EPSILON;
}

void Controllable::AlignWithMovement(float timeStep)
{
    Quaternion rot{node_->GetRotation()};
    Quaternion targetRot{};
    targetRot.FromLookRotation(move_);
    rot = rot.Slerp(targetRot, Clamp(timeStep * 23.f, 0.f, 1.f));
    node_->SetRotation(rot);
}

void Controllable::AlignWithVelocity(float timeStep)
{
    Quaternion targetRot{};
    Quaternion rot{node_->GetRotation()};
    targetRot.FromLookRotation(rigidBody_->GetLinearVelocity());
    ClampPitch(targetRot);
    float horizontalVelocity{(rigidBody_->GetLinearVelocity() * Vector3(1.f, 0.f, 1.f)).Length()};
    node_->SetRotation(rot.Slerp(targetRot, Clamp(7.f * timeStep * horizontalVelocity, 0.f, 1.f)));
}

void Controllable::ClampPitch(Quaternion& rot)
{
    float maxCorrection{ rot.EulerAngles().x_ - maxPitch_ };

    if (maxCorrection > 0.f)
        rot = Quaternion(-maxCorrection, node_->GetRight()) * rot;

    float minCorrection{rot.EulerAngles().x_ - minPitch_};

    if (minCorrection < 0.f)
        rot = Quaternion(-minCorrection, node_->GetRight()) * rot;
}

void Controllable::ClearControl()
{
    ResetInput();
}

Player* Controllable::GetPlayer()
{
    return INPUTMASTER->GetPlayerByControllable(this);
}
