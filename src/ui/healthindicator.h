/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HEALTHINDICATOR_H
#define HEALTHINDICATOR_H

#include "../luckey.h"

#define NUM_HEARTS 4
using Heart = Pair<Sprite*, Sprite*>;

class HealthIndicator: public Sprite
{
    DRY_OBJECT(HealthIndicator, Sprite);

public:
    static void RegisterObject(Context* context);
    HealthIndicator(Context* context);

    void Hit(StringHash /*eventType*/, VariantMap& /*eventData*/);
    void SetHealth(int hp, bool animate = true);
    int GetHealth() const { return health_; }

private:
    Color HealthToColor(int hp);
    float HealthToRotation(int hp);

    void HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/) { UpdateSizes(); }
    void UpdateSizes();

    PODVector<Heart> hearts_;
    int health_;
};

#endif // HEALTHINDICATOR_H
