/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainmenu.h"
#include "../game.h"
#include "../scores.h"
#include "../inputmaster.h"

#include "sardinerunmenu.h"

SardineRunMenu::SardineRunMenu(Context* context): GameMenu(context),
    buttons_{},
    arrows_{},
    panels_{},
    cells_{ 1 },
    page_{},
    lastPage_{},
    activePanel_{}
{
    SetVisible(false);
    SetAlignment(HA_CENTER, VA_TOP);

    CreatePanels();
    CreateButtons();

    UpdateSizes();
    SetLevelPickerVisible(true);
    SetActivePanel(0);

    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(SardineRunMenu, HandleGameStatusChanged));
}

void SardineRunMenu::CreatePanels()
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    StringVector mapFiles{};

    const String subFolder{ "Maps/Run/" };
    const String scanDir{ cache->GetResourceDirs().Front() + subFolder };
    fs->ScanDir(mapFiles, scanDir, "*.emp", SCAN_FILES, false);

    for (const String& mapFile: mapFiles)
    {
        const String mapName{ ReplaceExtension(mapFile, "") };
        const String fullMapName{ subFolder + mapName };

        Window* panel{ CreateChild<Window>() };
        panel->SetName(Scores::ScoreLevelName(fullMapName));
        panel->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
        panel->SetAlignment(HA_CENTER, VA_TOP);
        panel->SetOpacity(OCTAL(7));
        panel->SetVar("Level", fullMapName);
        panel->SetFocusMode(FM_FOCUSABLE);

        Text* mapNameText{ panel->CreateChild<Text>() };
        mapNameText->SetName("MapName");
        mapNameText->SetFont(FONT_TOYS_TOSS);
        mapNameText->SetAlignment(HA_LEFT, VA_TOP);
        mapNameText->SetText(mapName);

        Window* scoreList{ panel->CreateChild<Window>() };
        scoreList->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
        scoreList->SetName("ScoreList");
        scoreList->SetAlignment(HA_RIGHT, VA_BOTTOM);
        SubscribeToEvent(scoreList, E_CLICK, DRY_HANDLER(SardineRunMenu, HandlePanelClicked));

        panels_.Push(panel);
        UpdateScoreList(fullMapName);

        SubscribeToEvent(panel, E_CLICK, DRY_HANDLER(SardineRunMenu, HandlePanelClicked));
    }
}

void SardineRunMenu::UpdateScoreList(const String& level)
{
    Window* levelPanel{ GetPanelFromLevel(level) };
    if (!levelPanel)
        return;

    Window* scoreList{ levelPanel->GetChildStaticCast<Window>({ "ScoreList" }, false)};
    if (!scoreList)
        return;

    scoreList->RemoveAllChildren();
    const PODVector<int> scores{ GetSubsystem<Scores>()->GetScores(level) };

    if (scores.Size())
    {
        for (unsigned s{ 0u }; s < Min(5u, scores.Size()); ++s)
        {
            const int score{ scores.At(s) };
            Text* scoreText{ scoreList->CreateChild<Text>() };
            MarkupScoreText(scoreText);
            scoreText->SetText(GUI::SecondsToTimeCode(score / 1000.f, true));
        }
    }
    else
    {
        Text* noScoreText{ scoreList->CreateChild<Text>() };
        MarkupScoreText(noScoreText);
        noScoreText->SetText("--:--:--");
    }
}

Window* SardineRunMenu::GetPanelFromLevel(const String& level)
{
    for (Window* p: panels_)
        if (p->GetName() == Scores::ScoreLevelName(level))
            return p;

    return nullptr;
}

void SardineRunMenu::CreateButtons()
{
    for (int b{ 0 }; b < SRMB_ALL; ++b)
    {
        String text{ buttonsText_.At(b) };
        Button* button{ CreateTextButton(text) };
        button->SetVisible(!text.IsEmpty());
        buttons_.Insert({ b, button });
    }

    for (int b{ SRMB_CONTINUE }; b <= SRMB_MAIN; ++b)
    {
        Button* button{ buttons_[b] };

        if (b != SRMB_CONTINUE)
        {
            buttons_[b - 1]->SetVar("Next", button);
            button->SetVar("Prev", buttons_[b - 1]);
        }

        if (b == SRMB_MAIN)
        {
            button->SetVar("Next", buttons_[SRMB_CONTINUE]);
            buttons_[SRMB_CONTINUE]->SetVar("Prev", button);
        }
    }

    Button* startButton{ buttons_[SRMB_START] };
    SubscribeToEvent(startButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleStartButtonClicked));
    Button* backButton{ buttons_[SRMB_BACK] };
    SubscribeToEvent(backButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleBackButtonClicked));

    startButton->SetVar("Right", backButton);
    startButton->SetVar("Left" , backButton);
    backButton ->SetVar("Right", startButton);
    backButton ->SetVar("Left" , startButton);

    for (Window* panel: panels_)
    {
        panel->SetVar("Next", startButton);
        panel->SetVar("Prev", startButton);
    }

    Button* continueButton{ buttons_[SRMB_CONTINUE] };
    SubscribeToEvent(continueButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleContinueButtonClicked));
    Button* restartButton{ buttons_[SRMB_RESTART] };
    SubscribeToEvent(restartButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleRestartButtonClicked));
    Button* levelButton{ buttons_[SRMB_LEVEL] };
    SubscribeToEvent(levelButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleLevelButtonClicked));
    Button* settingsButton{ buttons_[SRMB_SETTINGS] };
    SubscribeToEvent(settingsButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleSettingsButtonClicked));
    Button* mainButton{ buttons_[SRMB_MAIN] };
    SubscribeToEvent(mainButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleMainMenuButtonClicked));

    for (bool left: { true, false })
    {
        Button* arrowButton{ CreateChild<Button>() };
        arrowButton->SetAlignment(HA_CENTER, VA_CENTER);
        arrowButton->SetFocusMode(FM_RESETFOCUS);
        arrowButton->SetTexture(RES(Texture2D, "UI/Arrow.png"));
        arrowButton->SetBlendMode(BLEND_ALPHA);
        arrowButton->SetImageRect({ 0 + 128 * !left, 0, 128 + 128 * !left, 256 });
        arrowButton->SetHoverOffset(0, 256);

        SubscribeToEvent(arrowButton, E_CLICK, DRY_HANDLER(SardineRunMenu, HandleButtonClicked));
        SubscribeToEvent(arrowButton, E_CLICKEND, DRY_HANDLER(SardineRunMenu, HandleArrowClicked));
        arrows_.Push(arrowButton);
    }
}

void SardineRunMenu::SetLevelPickerVisible(bool visible)
{
    for (int b{ 0 }; b < SRMB_ALL; ++b)
    {
        bool pickerButton{ b >= SRMB_START};

        Button* button{ buttons_[b] };
        button->SetVisible( visible ^ !pickerButton);
    }

    for (unsigned p{ 0 }; p < panels_.Size(); ++p)
    {
        Window* panel{ panels_.At(p) };
        panel->SetVisible(visible && p < cells_);
    }

    for (Button* arrow: arrows_)
        arrow->SetVisible(visible);

    if (visible)
    {
        firstElem_ = buttons_[SRMB_START];
        lastElem_ = buttons_[SRMB_BACK];
    }
    else
    {
        firstElem_ = buttons_[SRMB_CONTINUE];
        lastElem_ = buttons_[SRMB_MAIN];
    }

    GetSubsystem<UI>()->SetFocusElement(firstElem_);
}

void SardineRunMenu::SetActivePanel(unsigned index)
{
    const unsigned page{ index / cells_ };
    if (page_ != page)
        SetPage(page);

    for (unsigned p{ 0 }; p < panels_.Size(); ++p)
    {
        UI* ui{ GetSubsystem<UI>() };

        Window* panel{ panels_.At(p) };
        const bool active{ (p == index) };
        const int d{ active * 16};
        const IntRect rect{ IntRect{ 48, 0, 64, 16 } + IntRect{ d, d, d, d } };
        panel->SetImageRect(rect);
        panel->GetChildStaticCast<Window>(String{ "ScoreList" })->SetImageRect(rect);
        panel->SetOpacity((active ? .9f : .7f));

        if (active && ui->GetFocusElement() != panel)
        {
            ui->SetFocusElement(panel);

            for (Button* b: { buttons_[SRMB_BACK], buttons_[SRMB_START] })
            {
                b->SetVar("Next", panel);
                b->SetVar("Prev", panel);
            }
        }
    }

    activePanel_ = index;
}

void SardineRunMenu::SetPage(unsigned page)
{
    page_ = page;

    const unsigned c{ cells_ * page };
    const Pair<unsigned, unsigned> range{ c, c + cells_ - 1u };

    for (unsigned p{ 0 }; p < panels_.Size(); ++p)
    {
        Window* panel{ panels_.At(p) };
        const bool visible{ p >= range.first_ && p <= range.second_ };
        panel->SetVisible(visible);
    }

    if (activePanel_ < range.first_ || activePanel_ > range.second_)
        SetActivePanel(Min(panels_.Size() - 1, activePanel_ % cells_ + range.first_));
}

void SardineRunMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    Game* game{ GetSubsystem<Game>() };
    int shiftY{ -GUI::sizes_.logo_ / 4 * (game->GetStatus() == GS_PAUSED) };

    IntVector2 panelSize{ VectorCeilToInt(Vector2{ GUI::VMin(40), GUI::VMin(55) }) };
    IntVector2 cellSize{ panelSize + VectorCeilToInt(Vector2{ GUI::VMin(5), GUI::VMin(5) }) };

    const int columns{ Min(Max(1, (GetWidth() - panelSize.x_ * .5f) / cellSize.x_), panels_.Size()) };
    const int rows{ Min(Max(1, (GetHeight() - (cellSize.y_ + GUI::sizes_.logo_) / 2) / cellSize.y_), panels_.Size() / columns) };
    cells_ = columns * rows;
    page_ = activePanel_ / cells_;
    lastPage_ = (panels_.Size() - 1) / cells_;

    cellSize = VectorCeilToInt(Vector2{ GUI::VW(80) / columns, (GUI::VH(80) - GUI::sizes_.logo_ / 2) / rows });
    panelSize = cellSize - VectorCeilToInt(Vector2{ GUI::VW(2), GUI::VH(3) });

    for (unsigned p{ 0 }; p < panels_.Size(); ++p)
    {
        const int c{ static_cast<int>(p % cells_) };
        const int column{ c % columns };
        const int row{ c / columns };

        Window* panel{ panels_.At(p) };
        panel->SetPosition((-columns / 2 + ((columns + 1) % 2) * .5f + column) * cellSize.x_,
                           row * cellSize.y_ + GUI::sizes_.logo_ * .55f + GUI::VH() + shiftY);
        panel->SetSize(panelSize);
        if (arrows_.Front()->IsVisible())
            panel->SetVisible(p >= page_ * cells_ && p < cells_ + page_ * cells_);

        Text* mapNameText{ panel->GetChildStaticCast<Text>(String{ "MapName" }) };
        const float fontSize{ (panelSize.x_ + panelSize.y_) / 23.f };
        mapNameText->SetFontSize(fontSize);
        mapNameText->SetPosition(fontSize / 2, fontSize / 3);

        Window* scoreList{ panel->GetChildStaticCast<Window>(String{ "ScoreList" }) };
        scoreList->SetPosition(-GUI::VMin(2), -GUI::VMin(2));
        scoreList->SetSize(Min(panelSize.x_, panelSize.y_) / 3, panelSize.y_ / 2);

        const Vector<SharedPtr<UIElement> >& children{ scoreList->GetChildren() };
        for (unsigned i{ 0 }; i < children.Size(); ++i)
        {
            Text* scoreText{ static_cast<Text*>(children.At(i).Get()) };
            const float scoreSize{ Min(panelSize.x_, panelSize.y_) / 20.f };
            scoreText->SetFontSize(scoreSize);
            scoreText->SetPosition(-scoreSize / 2, scoreSize / 2 * (1 + i * 3.4f));
        }
    }

    for (int b{ 0 }; b < SRMB_ALL; ++b)
    {
        bool pickerButton{ b >= SRMB_START};

        Button* button{ buttons_[b] };
        const float buttonHeight{ GUI::VMin(8) + GUI::VMin(4) };
        button->SetSize((pickerButton ? GUI::VW(30) : GUI::VMin(60)), buttonHeight);

        if (!pickerButton)
        {
            button->SetPosition(0, GUI::sizes_.logo_ * 3/5 + (buttonHeight + GUI::VH(1)) * (b - 1));
        }
        else
        {
            bool left{ b == SRMB_BACK };
            button->SetPosition(RoundToInt((GUI::VW(42)) * (.5f - left)),
                                GUI::VH(90) - .5f * buttonHeight + shiftY);
        }

        Text* buttonText{ button->GetChildStaticCast<Text>(0u) };
        const float fontSize{ buttonHeight * .55f };
        buttonText->SetFontSize(fontSize);
        buttonText->SetPosition(0, fontSize / 11);
        buttonText->SetEffectStrokeThickness(1 + Ceil(fontSize / 23));
    }

    for (Button* arrowButton: arrows_)
    {
        arrowButton->SetSize(Min(Min(GUI::VH(11), GUI::VW(15)), 128), Min(Min(GUI::VH(22), GUI::VW(30)), 256));
        arrowButton->SetPosition(GUI::VW(42) * (1 - 2 * (arrowButton == arrows_.Front())), -GUI::VH(1));
    }
}

void SardineRunMenu::Hide()
{
    SetVisible(false);

    Game* game{ GetSubsystem<Game>() };
    if (game->GetStatus() == GS_MAIN)
        GetSubsystem<GUI>()->GetMainMenu()->SetButtonsVisible(true);
    else if (game->GetStatus() == GS_PAUSED)
        game->TogglePause();
}

void SardineRunMenu::HandlePanelClicked(StringHash eventType, VariantMap& eventData)
{
    UIElement* element{ static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr()) };

    if (element->GetName() == "ScoreList")
        element = element->GetParent();

    for (unsigned p{ 0 }; p < panels_.Size(); ++p)
    {
        if (panels_.At(p) == element)
        {
            SetActivePanel(p);
            return;
        }
    }
}

void SardineRunMenu::HandleArrowClicked(StringHash eventType, VariantMap& eventData)
{
    unsigned page{ page_ };
    if (arrows_[0] == eventData[Click::P_ELEMENT].GetPtr())
    {
        if (page_ > 0)
            --page;
        else
            page = lastPage_;
    }
    else
    {
        if (page_ < lastPage_)
            ++page;
        else
            page = 0;
    }

    if (page_ != page)
        SetPage(page);
}

void SardineRunMenu::HandleMenuInput(StringHash eventType, VariantMap& eventData)
{
    GameMenu::HandleMenuInput(eventType, eventData);

    if (!IsVisible())
        return;

    const VariantVector& varVec{ eventData[MenuInput::P_ACTIONS].GetVariantVector() };
    PODVector<MasterInputAction> actions{};
    for (const Variant& a: varVec)
        actions.Push(static_cast<MasterInputAction>(a.GetUInt()));

    int pick{ actions.Contains(MIA_INCREMENT)
            - actions.Contains(MIA_DECREMENT) };

    UI* ui{ GetSubsystem<UI>() };
    const bool panelFocus{ panels_.Contains(static_cast<Window*>(ui->GetFocusElement())) };
    if (panelFocus)
    {
        if (actions.Contains(MIA_CONFIRM))
        {
            ui->SetFocusElement(buttons_[SRMB_START]);
            return;
        }

        pick += actions.Contains(MIA_RIGHT)
              - actions.Contains(MIA_LEFT);
    }

    if (pick != 0)
    {
        const unsigned numPanels{ panels_.Size() };
        int index{ pick + static_cast<int>(activePanel_)};
        while (index < 0)
            index += numPanels;
        index %= numPanels;

        SetActivePanel(index);
    }

}

void SardineRunMenu::HandleStartButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<Game>()->StartNew(GM_RUN, panels_.At(activePanel_)->GetVar("Level").GetString());

    Hide();
}

void SardineRunMenu::HandleBackButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    Game* game{ GetSubsystem<Game>() };
    if (game->GetStatus() == GS_MAIN)
        Hide();
    else
        SetLevelPickerVisible(false);
}

void SardineRunMenu::HandleContinueButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    Hide();
}

void SardineRunMenu::HandleRestartButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<Game>()->StartNew(GM_RUN, GetGlobalVar("Level").GetString());

    Hide();
}

void SardineRunMenu::HandleLevelButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetLevelPickerVisible(true);
}

void SardineRunMenu::HandleSettingsButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetVisible(false);
    GetSubsystem<GUI>()->ShowSettings();
}

void SardineRunMenu::HandleMainMenuButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    Game* game{ GetSubsystem<Game>() };
    game->SetStatus(GS_MAIN);

    Hide();
}

void SardineRunMenu::HandleGameStatusChanged(StringHash eventType, VariantMap& eventData)
{
    GameStatus status{ static_cast<GameStatus>(eventData[GameStatusChanged::P_STATUS].GetInt()) };

    if (status == GS_MAIN)
        SetLevelPickerVisible(true);
    else
        SetLevelPickerVisible(false);

    UpdateSizes();
}
