/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"

#include "mainmenu.h"

MainMenu::MainMenu(Context* context): GameMenu(context),
    tux_{ nullptr },
    logo_{ nullptr },
    buttons_{}
{
    SetAlignment(HA_CENTER, VA_TOP);

    tux_ = CreateChild<Sprite>();
    tux_->SetTexture(RES(Texture2D, "UI/Tux.png"));
    tux_->SetAlignment(HA_RIGHT, VA_BOTTOM);
    tux_->SetBlendMode(BLEND_ALPHA);
    tux_->SetSize(256, 256);

    logo_ = CreateChild<Sprite>();
    logo_->SetTexture(RES(Texture2D, "UI/Logo.png"));
    logo_->SetAlignment(HA_CENTER, VA_TOP);
    logo_->SetBlendMode(BLEND_ALPHA);

    for (int b{ 0 }; b < MMB_ALL; ++b)
    {
        String text{ buttonsText_.At(b) };
        Button* button{ CreateTextButton(text) };
        buttons_.Insert({ b, button });

        if (b == MMB_STORY)
        {
            button->SetOpacity(.5f);
            button->SetEnabled(false);
        }

        if (b != 0)
        {
            buttons_[b - 1]->SetVar("Next", button);
            button->SetVar("Prev", buttons_[b - 1]);
        }

        if (b == MMB_ALL - 1)
        {
            button->SetVar("Next", buttons_[0]);
            buttons_[0]->SetVar("Prev", button);
        }
    }

    firstElem_ = buttons_[MMB_SARDINE];
    lastElem_  = buttons_[MMB_EXIT];
    lastElem_->AddTag("Silent");

    SubscribeToEvent(buttons_[MMB_SARDINE],  E_CLICKEND, DRY_HANDLER(MainMenu, HandleSardineRunButtonClicked));
    SubscribeToEvent(buttons_[MMB_SETTINGS], E_CLICKEND, DRY_HANDLER(MainMenu, HandleSettingsButtonClicked));
    SubscribeToEvent(buttons_[MMB_EXIT],     E_CLICKEND, DRY_HANDLER(MainMenu, HandleExitButtonClicked));

    UpdateSizes();
}

void MainMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    tux_->SetPosition(-tux_->GetWidth() - GUI::VMin(5), -tux_->GetHeight());

    logo_->SetPosition(-GUI::sizes_.logo_ / 2, GUI::VH(1));
    logo_->SetSize(GUI::sizes_.logo_, GUI::sizes_.logo_ / 2);

    for (int b{ 0 }; b < MMB_ALL; ++b)
    {
        Button* button{ buttons_[b] };
        const float buttonHeight{ GUI::VMin(8) + GUI::VMin(4) };
        button->SetSize(GUI::VMin(60), buttonHeight);
        button->SetPosition(0, GUI::sizes_.logo_ * 3/5 + (buttonHeight + GUI::VH(1)) * b);

        Text* buttonText{ button->GetChildStaticCast<Text>(0u) };
        float fontSize{ buttonHeight * .55f };
        buttonText->SetFontSize(fontSize);
        buttonText->SetPosition(0, fontSize / 11);
        buttonText->SetEffectStrokeThickness(1 + Ceil(fontSize / 23));
    }
}

void MainMenu::SetButtonsVisible(bool visible)
{
    for (Button* b: buttons_.Values())
        b->SetVisible(visible);
}

void MainMenu::HandleMenuInput(StringHash eventType, VariantMap& eventData)
{
    //Main menu still shows logo and Tux, so block event when buttons not visible
    if (!buttons_.Values().Front()->IsVisible())
        return;

    GameMenu::HandleMenuInput(eventType, eventData);
}

void MainMenu::HandleSardineRunButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetButtonsVisible(false);
    GetSubsystem<GUI>()->ShowSardineRun();
}

void MainMenu::HandleSettingsButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetButtonsVisible(false);
    GetSubsystem<GUI>()->ShowSettings();
}

void MainMenu::HandleExitButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<MasterControl>()->Exit();
}
