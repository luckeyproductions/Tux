/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainmenu.h"
#include "settingsmenu.h"
#include "sardinerunmenu.h"
#include "dash.h"
#include "../game.h"

#include "gui.h"

String GUI::SecondsToTimeCode(float time, bool hundredths)
{
    int minutes{};
    int seconds{};

    while (time >= 60.f)
    {
        time -= 60.f;
        ++minutes;
    }
    while (time >= 1.f)
    {
        time -= 1.f;
        ++seconds;
    }

    String secondsString{ String{ seconds } };
    if (seconds < 10)
        secondsString.Insert(0, '0');

    String timeCode{ String{ minutes } + ":" + secondsString};

    if (hundredths)
    {
        int hundredths{};
        while (time >= (.01f - M_EPSILON))
        {
            time -= .01f;
            ++hundredths;
        }

        String hundredthsString{ String{ hundredths } };
        if (hundredths < 10)
            hundredthsString.Insert(0, '0');

        timeCode.Append(":" + hundredthsString);
    }

    return timeCode;
}

UISizes GUI::sizes_{};

GUI::GUI(Context* context): Object(context),
    mainMenu_{ nullptr },
    settingsMenu_{ nullptr },
    sardineRunMenu_{ nullptr },
    dash_{ nullptr }
{
    context_->RegisterFactory<MainMenu>();
    context_->RegisterFactory<SettingsMenu>();
    context_->RegisterFactory<SardineRunMenu>();
    context_->RegisterFactory<Dash>();

    UpdateSizes();

    UIElement* root{ GetSubsystem<UI>()->GetRoot() };
    mainMenu_       = root->CreateChild<MainMenu>();
    settingsMenu_   = root->CreateChild<SettingsMenu>();
    sardineRunMenu_ = root->CreateChild<SardineRunMenu>();
    dash_           = root->CreateChild<Dash>();
    context_->RegisterSubsystem(dash_);

    SetMouseVisible(true);

    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(GUI, HandleScreenMode));
    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(GUI, HandleGameStatusChanged));
}

void GUI::SetMouseVisible(bool visible)
{
    Input* input{ GetSubsystem<Input>() };
    input->SetMouseVisible(visible);

    if (visible)
        input->SetMouseMode(MM_ABSOLUTE);
    else
        input->SetMouseMode(MM_WRAP);
}

void GUI::ShowSettings()
{
    settingsMenu_->SetVisible(true);
    GetSubsystem<UI>()->SetFocusElement(settingsMenu_->BackButton());
}

void GUI::ShowSardineRun()
{
    sardineRunMenu_->SetVisible(true);

    if (GetSubsystem<Game>()->GetStatus() == GS_PAUSED)
        sardineRunMenu_->SetLevelPickerVisible(false);

    SetMouseVisible(true);
}

void GUI::HandleCancel()
{
    if (sardineRunMenu_->IsVisible())
        sardineRunMenu_->Hide();
    else if (settingsMenu_->IsVisible())
        settingsMenu_->Hide();
}

void GUI::UpdateSizes()
{
    UISizes defaultSizes{};

    Graphics* graphics{ GetSubsystem<Graphics>() };
    sizes_.screen_ = graphics->GetSize();
    sizes_.logo_ = Min(Min(VMin(90), 1024), VH(55));

    if (sizes_ != defaultSizes)
        SendEvent(E_UISIZESCHANGED);
}

void GUI::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateSizes();

    UIElement* root{ GetSubsystem<UI>()->GetRoot() };

    for (int i{ 0 }; i <= 6; ++i)
        for (UIElement* elem: root->GetChildrenWithTag(i != 0 ? "h" + String{ i } : "p", true))
            static_cast<Text*>(elem)->SetFontSize(sizes_.text_[i]);
}

void GUI::HandleGameStatusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    GameStatus oldStatus{ static_cast<GameStatus>(eventData[GameStatusChanged::P_OLDSTATUS].GetInt()) };
    GameStatus status{ static_cast<GameStatus>(eventData[GameStatusChanged::P_STATUS].GetInt()) };

    if (oldStatus == GS_MAIN)
    {
        mainMenu_->SetVisible(false);
        SetMouseVisible(false);

    }
    else if (status == GS_MAIN)
    {
        mainMenu_->SetVisible(true);
        SetMouseVisible(true);

        Dash* dash{ GetSubsystem<Dash>() };
        dash->SetVisible(false);
    }

    if (status == GS_PLAY)
    {
        SetMouseVisible(false);
    }
    else if (status == GS_PAUSED)
    {
        sardineRunMenu_->SetVisible(true);
        SetMouseVisible(true);
    }
}
