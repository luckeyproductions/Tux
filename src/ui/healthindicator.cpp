/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../tux.h"
#include "gui.h"
#include "../effectmaster.h"

#include "healthindicator.h"

void HealthIndicator::RegisterObject(Context* context)
{
    context->RegisterFactory<HealthIndicator>();

    DRY_COPY_BASE_ATTRIBUTES(Sprite);
}

HealthIndicator::HealthIndicator(Context* context): Sprite(context),
    hearts_{},
    health_{ 4 }
{
    context_->RegisterSubsystem(this);

    SharedPtr<Texture2D> heartTexture{ RES(Texture2D, "UI/Heart.png") };
    for (int h{ 0 }; h < NUM_HEARTS; ++h)
    {
        Sprite* heartOutline{ CreateChild<Sprite>() };
        heartOutline->SetTexture(heartTexture);
        heartOutline->SetImageRect({ 512, 0, 1024, 512 });
        heartOutline->SetSize(96, 96);
        heartOutline->SetPosition(Vector2::ZERO);
        heartOutline->SetRotation(h * 90);
        heartOutline->SetBlendMode(BLEND_ALPHA);

        Sprite* heartFill{ heartOutline->CreateChild<Sprite>() };
        heartFill->SetTexture(heartTexture);
        heartFill->SetImageRect({ 0, 0, 512, 512 });
        heartFill->SetSize(96, 96);
        heartFill->SetBlendMode(BLEND_PREMULALPHA);
        heartFill->SetColor(HealthToColor(health_));

        hearts_.Push({ heartOutline, heartFill });
    }

    SetRotation(HealthToRotation(health_));

    SubscribeToEvent(E_PLAYERHIT, DRY_HANDLER(HealthIndicator, Hit));
    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(HealthIndicator, HandleSizesChanged));

    UpdateSizes();
}

void HealthIndicator::Hit(StringHash, VariantMap&)
{
    SetHealth(health_ - 1);
}

void HealthIndicator::SetHealth(int hp, bool animate)
{
    hp = Clamp(hp, 0, 4);

    if (health_ == hp)
        return;

    for (int h{ 0 }; h < NUM_HEARTS; ++h)
    {
        const Color finalColor{ HealthToColor(hp) };
        const Heart& heart{ hearts_.At(h) };
        heart.first_->SetVisible(h < hp);

        if (animate)
        {
            SharedPtr<ValueAnimation> colAnim{ context_->CreateObject<ValueAnimation>() };
            colAnim->SetKeyFrame(0.f, heart.second_->GetColor(C_TOPLEFT));
            colAnim->SetKeyFrame(1.f, finalColor);

            heart.second_->SetAttributeAnimation("Color", colAnim, WM_CLAMP, 10.f);
        }
        else
        {
            heart.second_->RemoveAttributeAnimation("Color");
            heart.second_->SetColor(finalColor);
        }
    }

    const float rotation{ HealthToRotation(hp) };
    if (animate)
    {
        SharedPtr<ValueAnimation> rotate{ EFFECT->Spring(GetRotation(), rotation,
                                                         .23f, 9.f) };
        SetAttributeAnimation("Rotation", rotate, WM_CLAMP);
    }
    else
    {
        RemoveAttributeAnimation("Rotation");
        SetRotation(rotation);
    }

    health_ = hp;
}

Color HealthIndicator::HealthToColor(int hp)
{
    switch (hp)
    {
    default: case 4: return Color::GREEN.Lerp(Color::CHARTREUSE, .23f);
    case 3: return Color::YELLOW;
    case 2: return Color::ORANGE;
    case 1: return Color::RED;
    case 0: return Color::BLACK;
    }
}

float HealthIndicator::HealthToRotation(int hp)
{
    return 270.f - 45.f * hp;
}

void HealthIndicator::UpdateSizes()
{
    const float scale{ GUI::sizes_.screen_.y_ / 1080.f };
    const int heartSize{ hearts_.Front().first_->GetWidth() };
    SetScale(scale);
    SetPosition(scale * heartSize, -GUI::sizes_.screen_.y_ + (scale * heartSize * 8/3));
}
