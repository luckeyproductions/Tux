/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INVENTORY_H
#define INVENTORY_H

#include "../items/fedora.h"
#include "../items/herring.h"
#include "../items/key.h"

#include "../luckey.h"

class Inventory: public UIElement
{
    DRY_OBJECT(Inventory, UIElement);

public:
    static void RegisterObject(Context* context);
    Inventory(Context* context);

    void Reset();
    void AddItem(const String& itemName);
    bool RemoveItem(const String& itemName);
    bool ContainsItem(const String& itemName) const { return items_.Contains(itemName); }
    String GetSelected() const;
    void UseSelected();
    void Step(int step);

    void RemoveHatDependentItems(const String& hatModelName);

private:
    void Select(int index);
    void UpdateIcons();
    void HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/) { UpdateSizes(); }
    void UpdateSizes();

    StringVector GetUnique() const;

    int currentIndex_;
    BorderImage* selector_;
    PODVector<Sprite*> icons_;
    Vector<String> items_;
};

#endif // INVENTORY_H
