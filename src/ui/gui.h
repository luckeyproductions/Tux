/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GUI_H
#define GUI_H

#include "../luckey.h"

#define FONT_FTOONK RES(Font, "Fonts/Ftoonk.otf")
#define FONT_TOYS_TOSS RES(Font, "Fonts/ToysToss.otf")
#define FONT_BOLEH RES(Font, "Fonts/Boleh.otf")

class MainMenu;
class SettingsMenu;
class SardineRunMenu;
class Dash;

struct UISizes {
    IntVector2 screen_{};
    int margin_{ 4 };
    int text_[7] = { 12, 38, 34, 23, 17, 16, 14 };
    int logo_{ 512 };
    int centralColumnWidth_{ 512 };
    int toolButton_{ 64 };

    bool operator != (UISizes& rhs)
    {
        if (logo_ != rhs.logo_
         || centralColumnWidth_ != rhs.centralColumnWidth_
         || margin_ != rhs.margin_)
            return true;

        for (int t{ 0 }; t <= 6; ++t)
        {
            if (text_[t] != rhs.text_[t])
                return true;
        }

        return false;
    }
};

class GUI: public Object
{
    DRY_OBJECT(GUI, Object);

public:
    static String SecondsToTimeCode(float time, bool hundredths = false);
    static UISizes sizes_;

    static float VW(float c = 1.f) { return c * sizes_.screen_.x_ * .01f; }
    static float VH(float c = 1.f) { return c * sizes_.screen_.y_ * .01f; }
    static float VMin(float c = 1.f) { return c * Min(VW(), VH()); }
    static float VMax(float c = 1.f) { return c * Max(VW(), VH()); }
    static float ScreenRatio() { return VW() / VH(); }
    static bool Portrait() { return ScreenRatio() < .8f; }

    GUI(Context* context);

    MainMenu* GetMainMenu() const { return mainMenu_; }
    SettingsMenu* GetSettingsMenu() const { return settingsMenu_; }
    SardineRunMenu* GetSardineRunMenu() const { return sardineRunMenu_; }

    void ShowSettings();
    void ShowSardineRun();
    void HandleCancel();

private:
    void SetMouseVisible(bool visible);
    void UpdateSizes();
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);
    void HandleGameStatusChanged(StringHash eventType, VariantMap& eventData);

    MainMenu* mainMenu_;
    SettingsMenu* settingsMenu_;
    SardineRunMenu* sardineRunMenu_;
    Dash* dash_;
};

DRY_EVENT(E_UISIZESCHANGED, UISizeChanged)
{
}

#endif // GUI_H
