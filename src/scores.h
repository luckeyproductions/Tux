/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCORES_H
#define SCORES_H

#include "luckey.h"

#define SCOREPATH GetSubsystem<MasterControl>()->GetStoragePath() + "/Scores.json"

class Scores: public Object
{
    DRY_OBJECT(Scores, Object);

public:
    static String ScoreLevelName(const String& mapName);

    Scores(Context* context);

    PODVector<int> GetScores(const String& mapName);
    void AddScore(const String& mapName, int score);

    bool LoadJSON(const JSONValue& source);
    bool Save();

private:
    SharedPtr<JSONFile> scoreFile_;
};

#endif // SCORES_H
