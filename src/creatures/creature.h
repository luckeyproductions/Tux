/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CREATURE_H
#define CREATURE_H

#include "../controllable.h"

enum HitEffect{ HE_NONE = 0, HE_DIE, HE_STUN, HE_HURT, HE_SLIDE };

class Creature: public Controllable
{
    DRY_OBJECT(Creature, Controllable);

public:
    Creature(Context* context);

    ~Creature() = default;

    void OnNodeSet(Node* node) override;
    void Set(const Vector3& position, const Quaternion& rotation) override;
    void Disable() override;
    void Update(float timeStep) override;

    virtual void Halt();
    virtual void Stun();
    virtual void Freeze();
    virtual void Perish();
    virtual void WhipHit(const Vector3& direction);

    bool IsStunned() const { return stun_ > 0.f; }
    bool IsFrozen() const { return frozen_; }
    virtual void EndStun();

protected:
    virtual void CreateKinematicController();
    virtual PODVector<Vector3> GetAvailableDirections() const;
    virtual bool CliffCheck(const Vector3& direction = Vector3::ZERO) const;
    virtual Vector3 TargetCheck();


    virtual void StartSlide(const Vector3& direction) {}
    virtual void EndSlide() {}

    KinematicCharacterController* kinematicController_;

    float dirCheckRadius_;
    float dirCheckDist_;
    bool wide_;
    float speed_;
    float traveled_;
    float stun_;
    bool frozen_;
    bool chaseHerring_;
    HitEffect topHitEffect_;
    HitEffect sideHitEffect_;
    bool lethalStun_;
    bool sliding_;

private:
    void HandleNodeCollision(const StringHash eventType, VariantMap& eventData);
};

#endif // CREATURE_H
