/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ZOMBIESNAIL_H
#define ZOMBIESNAIL_H

#include "creature.h"

class Shell;

class ZombieSnail: public Creature
{
    DRY_OBJECT(ZombieSnail, Creature);

public:
    static void RegisterObject(Context* context);
    ZombieSnail(Context* context);

    void OnNodeSet(Node* node) override;
    void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY) override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void PostUpdate(float timeStep) override;

protected:
    void Stun() override;
    void EndStun() override;

    void StartSlide(const Vector3& direction) override;
    void EndSlide() override;

private:
    void AddAnimationTriggers();
    void HandleAnimationTrigger(const StringHash, VariantMap& eventData);
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    Shell* shell_;
};

#endif // ZOMBIESNAIL_H
