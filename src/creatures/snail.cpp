/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "shell.h"

#include "snail.h"

void ZombieSnail::RegisterObject(Context* context)
{
    context->RegisterFactory<ZombieSnail>();
    context->RegisterFactory<Shell>();
}

ZombieSnail::ZombieSnail(Context* context): Creature(context),
    shell_{ nullptr }
{
    dirCheckRadius_ = .5f;
    dirCheckDist_ = 1.05f;
    chaseHerring_ = false;
    topHitEffect_ = HE_STUN;
    sideHitEffect_ = HE_HURT;
}

void ZombieSnail::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Creature::OnNodeSet(node);

    model_->SetModel(RES(Model, "Models/ZombieSnail.mdl"));
    model_->SetMaterial(0, RES(Material, "Materials/VColOutlineGlossy.xml"));
    animCtrl_->PlayExclusive("Animations/ZombieSnail/Idle.ani", 0, true);
    AddAnimationTriggers();

    collisionShape_->SetConvexHull(RES(Model, "Models/Snail_COLLISION.mdl"));
    collisionShape_->SetMargin(.21f);

//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(ZombieSnail, HandlePostRenderUpdate));
}

void ZombieSnail::AddAnimationTriggers()
{
    SharedPtr<Animation> walkAnim{ RES(Animation, "Animations/ZombieSnail/Crawl.ani" ) };
    walkAnim->AddTrigger(.23f, true, {});
    walkAnim->AddTrigger(.6f, true, {});
    SubscribeToEvent(graphicsNode_, E_ANIMATIONTRIGGER, DRY_HANDLER(ZombieSnail, HandleAnimationTrigger));
}

void ZombieSnail::Set(const Vector3& position, const Quaternion& rotation)
{
    Creature::CreateKinematicController();
    kinematicController_->SetJumpSpeed(0.f);
    kinematicController_->SetMaxSlope(17.f);
    kinematicController_->SetStepHeight(.23f);

    Creature::Set(position, rotation);

    Halt();
}

void ZombieSnail::Stun()
{
    Creature::Stun();

    const String retractAnim{ "Animations/ZombieSnail/Retract.ani" };
    animCtrl_->PlayExclusive(retractAnim, 0, true, .1f);
    animCtrl_->SetSpeed(retractAnim, 3.4f);
    animCtrl_->SetLooped(retractAnim, false);

    sideHitEffect_ = HE_SLIDE;
}

void ZombieSnail::EndStun()
{
    Creature::EndStun();

    const String retractAnim{ "Animations/ZombieSnail/Retract.ani" };
    animCtrl_->PlayExclusive(retractAnim, 0, true, .1f);
    animCtrl_->SetTime(retractAnim, animCtrl_->GetLength(retractAnim));
    animCtrl_->SetSpeed(retractAnim, -2.3f);
    animCtrl_->SetLooped(retractAnim, false);

    sideHitEffect_ = HE_HURT;

    model_->SetMaterial(0, RES(Material, "Materials/VColOutlineGlossy.xml"));
}

void ZombieSnail::StartSlide(const Vector3& direction)
{
    rigidBody_->SetEnabled(false);
    collisionShape_->SetEnabled(false);

    Node* shellNode{ GetScene()->CreateChild("Shell") };
    shellNode->SetWorldPosition(node_->GetWorldPosition());
    shellNode->SetWorldDirection(direction);
    graphicsNode_->SetParent(shellNode);
    shellNode->CreateComponent<Shell>();
}

void ZombieSnail::EndSlide()
{
    rigidBody_->SetEnabled(true);
    collisionShape_->SetEnabled(true);

    kinematicController_->Warp(shell_->GetNode()->GetWorldPosition());
    graphicsNode_->SetParent(node_);
    shell_->GetNode()->Remove();
}

void ZombieSnail::Update(float timeStep)
{
    Creature::Update(timeStep);

    if (IsStunned())
    {
        if (stun_ < 2.f)
        {
            const float t{ 1.f - .23f * Sin(Abs(Mod(stun_, .5f) * 4.f - 1.f) * 180) };
            const String retractAnim{ "Animations/ZombieSnail/Retract.ani" };
            animCtrl_->SetTime(retractAnim, t * animCtrl_->GetLength(retractAnim));
        }

        return;
    }

    const float targetSpeed{ .5f * !Equals(move_.LengthSquared(), 0.f) };
    speed_ = Lerp(speed_, targetSpeed, timeStep);

    if (!Equals(speed_, 0.f))
    {
        float moveDot{ move_.Normalized().DotProduct(node_->GetWorldDirection()) };
        animCtrl_->PlayExclusive("Animations/ZombieSnail/Crawl.ani", 0, true, 2/6.f);
        animCtrl_->SetSpeed("Animations/ZombieSnail/Crawl.ani", moveDot * 8.f * Sqrt(kinematicController_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP).Length() * 2.3f));
    }
    else
    {
        animCtrl_->PlayExclusive("Animations/ZombieSnail/Idle.ani", 0, true, 1/6.f);
        animCtrl_->SetSpeed("Animations/ZombieSnail/Idle.ani", .5f);
    }
}

void ZombieSnail::FixedUpdate(float timeStep)
{
    if (sliding_)
    {
        if (!shell_->GetComponent<RigidBody>()->IsActive())
            EndSlide();
        return;
    }

    if (Equals(speed_, 0.f))
    {
        auto dirs{ GetAvailableDirections() };
        if (!dirs.IsEmpty())
        {
            const Vector3 dir{ VectorRound(node_->GetWorldDirection()) };
            if (Equals(dir.LengthSquared(), 1.f))
                SetMove(dirs.Contains(dir) ? dir : -dir);
            else
                SetMove(dirs.At(Random(dir.Length())));
        }
    }
    else if (!GetAvailableDirections().Contains(move_))
    {
        node_->Translate(-speed_ * node_->GetWorldDirection() * timeStep);

        if (move_.LengthSquared() > 0.f)
            Halt();
    }

    const Vector3 rightMove{ move_.CrossProduct(Vector3::UP) };
    const Vector3 roundPos{ VectorRound(node_->GetWorldPosition()) };
    const Vector3 offPos{ roundPos - node_->GetWorldPosition() };
    const Vector3 lineRight{ rightMove * rightMove.Normalized().DotProduct(offPos) };
    const Vector3 targetWalkDir{ (move_ + lineRight) * timeStep };
    kinematicController_->SetWalkDirection(speed_ * targetWalkDir);
}

void ZombieSnail::PostUpdate(float timeStep)
{
    const Vector3 oldPos{ node_->GetPosition() };
    const Vector3 newPos{ oldPos.Lerp(kinematicController_->GetPosition(), Min(1.f, 34.f * timeStep)) };
    const Vector3 delta{ newPos - oldPos };
    const float dist{ delta.Length() };

    node_->SetPosition(newPos);

    if (!Equals(speed_, 0.f))
    {
        if (dist < .1f * timeStep * speed_ || CliffCheck())
        {
            Halt();
        }
        else
        {
            const Vector3 dir{ node_->GetWorldDirection() };
            node_->Yaw(Equals(dir.Angle(delta), 180.f, 2.5f)
                       ? RandomSign() * 5.f
                       : (dir.CrossProduct(move_).y_ * timeStep * 420.f));
            kinematicController_->SetTransform(kinematicController_->GetPosition(), node_->GetWorldRotation());
        }
    }
}

void ZombieSnail::HandleAnimationTrigger(const StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    /// Samples need work
//    if (animCtrl_->GetTime("Animations/ZombieSnail/Crawl.ani") / animCtrl_->GetLength("Animations/ZombieSnail/Crawl.ani") < .5f)
//        PlaySample("SnailA.wav", 0.f, .2f, false);
//    else
//        PlaySample("SnailB.wav", 0.f, .3f, false);
}

void ZombieSnail::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    collisionShape_->DrawDebugGeometry(GetScene()->GetComponent<DebugRenderer>(), false);
}
