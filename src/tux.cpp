/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "spawnmaster.h"
#include "player.h"
#include "ui/healthindicator.h"
#include "ui/oxygenindicator.h"
#include "ui/inventory.h"
#include "ui/dash.h"
#include "environs/door.h"
#include "environs/teleporter.h"
#include "effect/stars.h"
#include "items/hardhat.h"
#include "items/wizardhat.h"
#include "blockmap/blockmap.h"

#include "tux.h"

void Tux::RegisterObject(Context* context)
{
    context->RegisterFactory<Tux>();
    context->RegisterFactory<TuxSliding>();
}

Tux::Tux(Context* context): Controllable(context),
    kinematicController_{ nullptr },
    slider_{ nullptr },
    hat_{ nullptr },
    fire_{ nullptr },
    walkSpeed_{ .5f },
    jumpEnded_{ false },
    sliding_{ false },
    clinging_{ false },
    sinceCling_{ 1.f },
    hold_{ nullptr },
    stun_{},
    burnt_{ false },
    leftCast_{ false },
    movingData_{}
{
}

void Tux::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Controllable::OnNodeSet(node);

    model_->SetModel(RES(Model, "Models/Tux.mdl"));
    model_->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
    model_->ApplyMaterialList();
    animCtrl_->PlayExclusive("Animations/Idle.ani", 0, true);
    animCtrl_->SetStartBone("Animations/Idle.ani", "Hips");
    AddAnimationTriggers();

    graphicsNode_->SetPosition(Vector3::UP * .05f);

    for (bool left: { true, false })
    {
        Node* eyeNode{ node_->GetChild(left ? "Eye.L" : "Eye.R", true) };
        StaticModel* eye{ eyeNode->CreateComponent<StaticModel>() };
        eye->SetModel(RES(Model, "Models/Eye.mdl"));
        eye->ApplyMaterialList();
    }

    rigidBody_->SetKinematic(true);
    rigidBody_->SetTrigger(true);
    rigidBody_->SetFriction(0.f);
    rigidBody_->SetCollisionLayer(LAYER(L_PLAYER));
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED - LAYER(L_PROJECTILE));
    collisionShape_->SetConvexHull(RES(Model, "Models/Tux_COLLISION.mdl"));
    collisionShape_->SetMargin(.1f);
//    collisionShape_->SetCapsule(.7f, 1.5f, .75f * Vector3::UP);

    kinematicController_ = node_->CreateComponent<KinematicCharacterController>();
    kinematicController_->SetTransform(node_->GetPosition(), node_->GetRotation());
    kinematicController_->SetGravity(GRAVITY);
    kinematicController_->SetJumpSpeed(7.f);
    kinematicController_->SetCollisionLayer(LAYER(L_PLAYER));
    kinematicController_->SetCollisionMask(M_MAX_UNSIGNED - LAYER(L_PROJECTILE));
    kinematicController_->SetMaxSlope(47.5f);
    kinematicController_->SetStepHeight(.17f);

    Node* headNode{ node_->GetChild("Head", true) };
    hat_ = headNode->CreateComponent<StaticModel>();
    hat_->SetCastShadows(true);

    Node* bubbleNode{ headNode->CreateChild("Bubbles") };
    bubbleNode->SetPosition({ .0f, .0f, .25f });

    Node* fireNode{ node_->GetChild("Hips", true)->CreateChild("Fire") };
    fireNode->SetPosition({ 0.f, -.23f, .23f });
    fire_ = fireNode->CreateComponent<ParticleEmitter>();
    fire_->SetEffect(RES(ParticleEffect, "Particles/Fire.xml"));
    fire_->SetEmitting(false);

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Tux, HandlePostRenderUpdate));
    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(Tux, HandleNodeCollision));
}


void Tux::SetHat(const String& hatName)
{
    Inventory* inventory{ GetSubsystem<Inventory>() };

    const String currentHatName{ CurrentHatName() };
    if (!CurrentHatName().IsEmpty())
    {
        if (hatName != currentHatName)
            inventory->RemoveHatDependentItems(currentHatName);
    }

    if (hatName.IsEmpty())
    {
        hat_->SetModel(nullptr);
        return;
    }

    hat_->SetModel(RES(Model, String{ "Models/" } + hatName + ".mdl"));
    hat_->ApplyMaterialList();

    if (hatName == "Fedora")
    {
        if (!inventory->ContainsItem("Bullwhip"))
            inventory->AddItem("Bullwhip");
    }
    else if (hatName == "HardHat")
    {
        for (int b{ 0 }; b < 5; ++b)
            inventory->AddItem("Bricks");

        if (!inventory->ContainsItem("Tools"))
            inventory->AddItem("Tools");
    }
    else if (hatName == "WizardHat")
    {
        if (!inventory->ContainsItem("IceSpell"))
            inventory->AddItem("IceSpell");
        if (!inventory->ContainsItem("FireSpell"))
            inventory->AddItem("FireSpell");
    }
}

String Tux::CurrentHatName() const
{
    Model* currentHat{ hat_->GetModel() };
    if (currentHat)
        return GetFileName(currentHat->GetName());
    else
        return "";
}

bool Tux::HasToughHat() const
{
    const String& hatName{ CurrentHatName() };
    return hatName.Contains("HardHat") || hatName.Contains("Helmet");
}

TuxSliding* Tux::StartSliding(bool launch)
{
    Controllable::HandleAction(PlayerInputAction::PIA_JUMP);

    animCtrl_->Stop("Animations/Cast.L.ani", .05f);
    animCtrl_->Stop("Animations/Cast.R.ani", .05f);

    kinematicController_->SetWalkDirection(Vector3::ZERO);
    kinematicController_->SetEnabled(false);
    rigidBody_->SetEnabled(false);
    collisionShape_->SetEnabled(false);

    Node* sliderNode{ GetScene()->CreateChild("SlidingTux") };
    sliderNode->SetWorldTransform(node_->GetWorldTransform());
    sliderNode->Translate(OFFSET_SLIDER);

    slider_ = sliderNode->CreateComponent<TuxSliding>();
    slider_->rigidBody_->SetLinearVelocity(rigidBody_->GetLinearVelocity() + launch * node_->GetDirection() * (2.f + 3.f * kinematicController_->OnGround()));
    slider_->tux_ = this;

    animCtrl_->PlayExclusive("Animations/Slide.ani", 0, true, .17f);
    animCtrl_->SetStartBone("Animations/Slide.ani", "Hips");

    animCtrl_->PlayExclusive("Animations/Swim.ani", 1, true, .23f);
    animCtrl_->SetStartBone("Animations/Swim.ani", "Hips");
    animCtrl_->SetWeight("Animations/Swim.ani", 0.f);

    graphicsNode_->SetParent(slider_->GetNode());
    graphicsNode_->SetPosition(OFFSET_GRAPHICS - OFFSET_SLIDER);
    graphicsNode_->SetRotation(Quaternion::IDENTITY);

    sliding_ = true;

    SubscribeToEvent(slider_, E_HARDBUMP, DRY_HANDLER(Tux, HandleHardBump));

    if (launch)
        slider_->Launch();

    return slider_;
}

void Tux::HandleHardBump(StringHash /*eventType*/, VariantMap& eventData)
{
    if (IsSliding() && !slider_->IsUnderwater())
        EndSliding();

    if (eventData[HardBump::P_STUN].GetBool())
    {
        if (!HasToughHat())
            Stun("Hit.wav");
        else
            PlaySample("Thud.wav", 0.f, .7f);
    }
    else if (eventData[HardBump::P_BURN].GetBool())
    {
        Burn();
    }
}

void Tux::EndSliding()
{
    rigidBody_->SetEnabled(true);
    collisionShape_->SetEnabled(true);

    kinematicController_->Warp(node_->GetPosition());
    kinematicController_->SetEnabled(true);
    kinematicController_->SetWalkDirection(slider_->rigidBody_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP) * .01f);
    kinematicController_->Jump(Vector3::UP * 1.7f);

    animCtrl_->Stop("Animations/Swim.ani", .11f);
    animCtrl_->PlayExclusive("Animations/Walk.ani", 0, true, .42f);
    animCtrl_->SetStartBone("Animations/Walk.ani", "Hips");

    node_->SetWorldPosition(graphicsNode_->GetWorldPosition() - OFFSET_SLIDER - OFFSET_GRAPHICS);
    node_->SetWorldDirection(graphicsNode_->GetWorldDirection().ProjectOntoPlane(Vector3::UP));
    graphicsNode_->SetParent(node_);
    graphicsNode_->SetPosition(OFFSET_GRAPHICS);
    graphicsNode_->SetRotation(Quaternion::IDENTITY);

    slider_->GetNode()->Remove();
    slider_ = nullptr;
    sliding_ = false;

    if (hold_)
    {
        if (hold_->GetType() == "Bullwhip")
            Release();
    }
}

void Tux::Burn()
{
    if (IsFainted())
        return;

    burnt_ = true;
    Stun("Sizzle.wav");

    if (!IsFainted())
        kinematicController_->Jump(Vector3::UP * (7.f + 2.f * !burnt_));
}

void Tux::Stun(const String& sampleName)
{
    if (IsStunned(true))
        return;

    if (sliding_)
        EndSliding();

    if (GetSubsystem<HealthIndicator>()->GetHealth() > 1)
    {
        stun_ = STUN_LENGTH;

        if (!burnt_)
        {
            animCtrl_->PlayExclusive("Animations/Stunned.ani", 0, true, .17f);
            animCtrl_->SetStartBone("Animations/Stunned.ani", "Hips");
            node_->GetChild("Head", true)->CreateChild("Stars")->CreateComponent<Stars>();
        }
        else
        {
            fire_->SetEmitting(true);
        }
    }
    else
    {
        stun_ = M_INFINITY;
        burnt_ = false;

        animCtrl_->PlayExclusive("Animations/Faint.ani", 0, true, .17f);
        animCtrl_->SetStartBone("Animations/Faint.ani", "Hips");
        animCtrl_->SetLooped("Animations/Faint.ani", false);

        GetSubsystem<Dash>()->StopTime();
    }

    SetMove(Vector3::ZERO);
    Release();

    if (!sampleName.IsEmpty())
        PlaySample(sampleName, 0.f, 1.7f);

    SendEvent(E_PLAYERHIT);
}

void Tux::EndStun(bool blink)
{
    stun_ = -1.7f * blink * !burnt_;

    Node* starNode{ graphicsNode_->GetChild("Head", true)->GetChild("Stars") };
    if (starNode)
        starNode->Remove();

    burnt_ = false;
    fire_->SetEmitting(false);
}

bool Tux::IsUnderwater() const
{
    if (IsSliding())
        return slider_->IsUnderwater();
    else
        return GetSubsystem<BlockMap>()->IsUnderwater(node_->GetWorldPosition() + Vector3::UP * .5f + OFFSET_SLIDER);
}

void Tux::PickUp(Item* item)
{
    const String itemName{ item->GetTypeName() };

    if (item->IsHat())
        SetHat(itemName);
    else if (itemName == "Life")
        GetSubsystem<HealthIndicator>()->SetHealth(4);
    else if (itemName == "Herring")
        GetSubsystem<Inventory>()->AddItem("Herring");
    else if (itemName == "Opener")
        GetSubsystem<Inventory>()->AddItem("Key");
}

bool Tux::CanUse(StringHash item)
{
    if (IsStunned() || IsClinging())
        return false;

    if (item == "Bullwhip")
    {
        return !IsSliding();
    }
    else if (item == "Bricks")
    {
        IntVector3 coords{ VectorRoundToInt(node_->GetWorldPosition() + node_->GetWorldDirection() * 1.25f + Vector3::UP * .5f) };
        return !IsSliding() && HardHat::AllowBrick(GetScene(), coords);
    }
    else if (item == "Tools")
    {
        IntVector3 coords{ VectorRoundToInt(node_->GetWorldPosition() + node_->GetWorldDirection() * 1.25f + Vector3::UP * .5f) };
        return !IsSliding() && HardHat::AllowChisel(coords);
    }
    else if (item == "IceSpell")
    {
        return !IsSliding();
    }
    else if (item == "FireSpell")
    {
        return !IsSliding();
    }
    else if (item == "Herring")
    {
        /// Check for obstacles?
        return true;
    }
    else if (item == "Key")
    {
        Door* nearestDoor{ GetSubsystem<SpawnMaster>()->GetNearest<Door>(node_->GetWorldPosition(), 1.7f) };

        if (nearestDoor)
            return true;
        else
            return false;
    }

    return false;
}

void Tux::UseItem(StringHash item)
{
    const Vector3 tuxPos{ node_->GetWorldPosition() };
    const Vector3 tuxDir{ node_->GetWorldDirection() };

    if (item == "Bullwhip")
    {
        Node* whipNode{ node_->CreateChild("Whip") };
        whipNode->Translate({ .4f, .8f, 1.2f });
        whipNode->LookAt(tuxPos + tuxDir * WHIP_REACH + node_->GetWorldUp() * WHIP_RADIUS, node_->GetWorldUp());
        Bullwhip* whip{ whipNode->CreateComponent<Bullwhip>() };
        hold_ = whip;
    }
    else if (item == "Bricks")
    {
        if (HardHat::LayBrick(GetScene(), tuxPos + tuxDir * 1.25f + Vector3::UP * .5f))
            PlaySample("Brick.wav", 1.f - Random(.05f), .75f, false);
    }
    else if (item == "Tools")
    {
        if (HardHat::Chisel(tuxPos + tuxDir * 1.25f + Vector3::UP * .5f))
            GetSubsystem<Inventory>()->AddItem("Bricks");

    }
    else if (item == "IceSpell")
    {
        PlaySample("IceCast.wav", 0.f, .6f, false);
        if (WizardHat::Freeze(GetScene(), { tuxPos + Vector3::UP, (tuxDir + Vector3::DOWN).Normalized() }))
            PlaySample("Freeze.wav", 0.f, .9f, false);

        for (const String& side: { ".R", ".L" })
        {
            const String ani{ "Animations/Cast" + side + ".ani" };
            const String startBone{ "Shoulder" + side };

            animCtrl_->Stop(ani, .25f);
            animCtrl_->Play(ani, 2, false, .1f);
            animCtrl_->SetStartBone(ani, startBone);
            animCtrl_->SetTime(ani, 0.f);
        }
    }
    else if (item == "FireSpell")
    {
        PlaySample("FireCast.wav", 1.f + RandomOffCenter(.05f), 1.5f, false);
        Fireball* fireball{ SPAWN->Create<Fireball>() };
        fireball->Set(tuxPos + Vector3::UP * .75f + tuxDir * .55f, node_->GetWorldRotation() * Quaternion{ Random(360.f), Vector3::FORWARD } );

        const String startBone{ "Shoulder." };
        const String side{ (leftCast_ ? "L" : "R") };
        const String ani{ "Animations/Cast." + side + ".ani" };
        animCtrl_->Stop(ani, .25f);
        animCtrl_->Play(ani, 2, false, .1f);
        animCtrl_->SetStartBone(ani, startBone + side);
        animCtrl_->SetTime(ani, 0.f);
        leftCast_ = !leftCast_;
    }
    else if (item == "Herring")
    {
        Node* herringNode{ GetScene()->CreateChild("RedHerring") };
        herringNode->SetWorldPosition(tuxPos + tuxDir);
        herringNode->SetWorldDirection(tuxDir);
        RedHerring* herring{ herringNode->CreateComponent<RedHerring>() };

        hold_ = herring;
    }
    else if (item == "Key")
    {
        Door* nearestDoor{ GetSubsystem<SpawnMaster>()->GetNearest<Door>(node_->GetWorldPosition(), 1.7f) };

        if (nearestDoor)
            nearestDoor->Open();
    }
}

void Tux::AddAnimationTriggers()
{
    SharedPtr<Animation> walkAnim{ RES(Animation, "Animations/Walk.ani" ) };
    walkAnim->AddTrigger(.4f, true, {});
    walkAnim->AddTrigger(.9f, true, {});
    SubscribeToEvent(graphicsNode_, E_ANIMATIONTRIGGER, DRY_HANDLER(Tux, HandleAnimationTrigger));
}

void Tux::FakeJump()
{
    if (IsStunned())
        return;

    if (!IsSliding())
    {
        animCtrl_->PlayExclusive("Animations/Jump.ani", 0, false, .1f + .1f * IsClinging());
        animCtrl_->SetStartBone("Animations/Jump.ani", "Hips");
        animCtrl_->SetSpeed("Animations/Jump.ani", .7f);
        animCtrl_->SetTime("Animations/Jump.ani", 0.f);
    }

    clinging_ = false;
    jumpEnded_ = true;
}

void Tux::HandleAction(int actionId)
{
    if (IsStunned())
        return;

    Controllable::HandleAction(actionId);

    switch (actionId)
    {
    default: return;
    case PIA_JUMP:
    {
        if (!IsUnderwater())
        {
            animCtrl_->PlayExclusive("Animations/Jump.ani", 0, false, .1f + .1f * IsClinging());
            animCtrl_->SetStartBone("Animations/Jump.ani", "Hips");
            animCtrl_->SetSpeed("Animations/Jump.ani", .7f);

            if (sliding_)
            {
                HandleAction(PIA_SLIDE);
                animCtrl_->SetTime("Animations/Jump.ani", animCtrl_->GetLength("Animations/Jump.ani") * .5f);
            }
            else if (kinematicController_->CanJump())
            {
                kinematicController_->Jump(/*Vector3::UP * 7.f*/);
                animCtrl_->SetTime("Animations/Jump.ani", 0.f);
            }
            else if (IsClinging())
            {
                kinematicController_->Jump(node_->GetWorldDirection() * .5f + Vector3::UP * 7.f);
                kinematicController_->SetWalkDirection(node_->GetWorldDirection() * .1f);
                clinging_ = false;
                sinceCling_ = .05f;
                jumpEnded_ = false;

                animCtrl_->SetTime("Animations/Jump.ani", 0.f);
            }
        }
    } break;
    case PIA_SLIDE:
    {
        if (!IsUnderwater())
        {
            if (!sliding_)
            {
                slider_ = StartSliding(true);
            }
            else
            {
                EndSliding();

                animCtrl_->PlayExclusive("Animations/Jump.ani", 0, false, .17f);
                animCtrl_->SetStartBone("Animations/Jump.ani", "Hips");
                animCtrl_->SetTime("Animations/Jump.ani", animCtrl_->GetLength("Animations/Jump.ani") * .5f);
            }
        }
    } break;
    case PIA_USE:
    {
        const Vector3 position{ node_->GetWorldPosition() };
        Teleporter* teleporter{ GetSubsystem<SpawnMaster>()->GetNearest<Teleporter>(position, .1f) };

        if (teleporter)
        {
            const Vector3 delta{ teleporter->GetOther()->GetNode()->GetWorldPosition() - teleporter->GetNode()->GetWorldPosition() };
            kinematicController_->Warp(position + delta);
            PlaySample("Warp.wav", 0.f, .7f);
            teleporter->Sparkle();
            teleporter->GetOther()->Sparkle();
        }
        else
        {
            GetSubsystem<Inventory>()->UseSelected();

            if (sliding_)
                Release();
        }

    } break;
    case PIA_RUN:
    {
        /* if not inverted run
        if (sliding_ && slider_->IsUnderwater())
            slider_->Dash();
        */
    }
    break;
    }
}

void Tux::EndAction(int actionId)
{
    switch (actionId)
    {
    default: break;
    case PIA_JUMP: jumpEnded_ = true;    break;
    case PIA_USE: if (hold_) Release(); break;
    case PIA_RUN:
    {
        // if inverted run
        if (sliding_ && slider_->IsUnderwater())
            slider_->Dash();
    }
    break;
    }
}

void Tux::Release()
{
    if (!hold_)
        return;

    if (hold_->GetType() == "Bullwhip")
    {
        hold_->GetNode()->Remove();
        hold_ = nullptr;

        if (IsSliding())
            EndSliding();
    }
    else if (hold_->GetType() == "RedHerring")
    {
        static_cast<RedHerring*>(hold_)->Activate();
        hold_ = nullptr;
    }
}

void Tux::Update(float timeStep)
{
    Controllable::Update(timeStep);

    if (!IsSliding())
    {
        if (IsUnderwater())
            StartSliding();
        else
            GetSubsystem<OxygenIndicator>()->SetShowOxygen(false);
    }

    if (IsStunned())
    {
        stun_ -=timeStep;

        if (stun_ <= 0.f)
            EndStun();
    }
    else if (stun_ < 0.f)
    {
        stun_ += timeStep;
        if (stun_ >= 0.f)
            stun_ = 0.f;

        model_->SetEnabled(Mod(PowN(-stun_, 2), .1f) < .05f);
    }

    const Quaternion camYawRot{ GetScene()->GetChild("TuxCam")->GetWorldRotation().YawAngle(), Vector3::UP };
    Vector3 move{ camYawRot * move_.ProjectOntoPlane(Vector3::UP) };
    if (burnt_)
    {
        actions_[PIA_RUN] = true;
        move = move.NormalizedOrDefault(node_->GetWorldDirection());
    }

    walkSpeed_ = Lerp(walkSpeed_, !sliding_ && move.Length() > .05f ? 2.3f : .42f, timeStep * 5.f);
    animCtrl_->SetSpeed("Animations/Walk.ani", 5.f * Sqrt(kinematicController_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP).Length() * 2.3f));

    if (sliding_)
    {
        node_->SetWorldPosition(slider_->GetNode()->GetWorldPosition());
        node_->SetWorldDirection(graphicsNode_->GetWorldDirection().ProjectOntoPlane(Vector3::UP).Normalized());
    }
    else if (!IsClinging() && (burnt_ || !IsStunned()))
    {
        const bool grounded{ kinematicController_->OnGround() };
        const Vector3 flatVelocity{ ((grounded && !hold_) ? move : rigidBody_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP) )};
        const Vector3 targetDirection{ flatVelocity.NormalizedOrDefault(node_->GetDirection()) };
        const float speed{ flatVelocity.Length() };

        if (speed > .1f)
        {
            const Vector3 flatDirection{ node_->GetDirection().ProjectOntoPlane(Vector3::UP) };
            node_->LookAt(node_->GetPosition() + flatDirection.Lerp(targetDirection, timeStep * walkSpeed_ * 7.f));
        }

        if (burnt_ || kinematicController_->CanJump())
        {
            if (burnt_ || speed > .23f || move.Length() > .1f)
            {
                animCtrl_->PlayExclusive("Animations/Walk.ani", 0, true, .17f);
                animCtrl_->SetStartBone("Animations/Walk.ani", "Hips");
            }
            else
            {
                animCtrl_->PlayExclusive("Animations/Idle.ani", 0, true, .12f);
                animCtrl_->SetStartBone("Animations/Idle.ani", "Hips");
            }
        }
    }
}

void Tux::FixedUpdate(float timeStep)
{
    if (!sliding_)
    {
        Vector3 move{ Quaternion{ GetScene()->GetChild("TuxCam")->GetWorldRotation().YawAngle(), Vector3::UP }
                            * move_.ProjectOntoPlane(Vector3::UP) };

        ClingCheck(move);

        if (!clinging_)
            sinceCling_ += timeStep;

        if (burnt_)
            move = move.NormalizedOrDefault(node_->GetWorldDirection());

        if (!IsClinging() && !hold_)
        {
            const float grip{ (kinematicController_->OnGround() ? .9f : .2f) + (1.f + .5f * move_.LengthSquared() == 0) };
            const Vector3 currentWalkDir{ kinematicController_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP)};
            const Vector3 targetWalkDir{ walkSpeed_ * move * (burnt_ || !IsStunned()) * timeStep * (1.2f * actions_[PIA_RUN] + 1.3f) };
            kinematicController_->SetWalkDirection(currentWalkDir.Lerp(targetWalkDir, Min(1.f, timeStep * 7.f) * grip));
        }
        else
        {
            kinematicController_->SetWalkDirection(Vector3::ZERO);
        }

        if (IsClinging())
        {
            kinematicController_->SetGravity(GRAVITY * .25f);
        }
        else
        {
            const PlayerInputAction j{ PlayerInputAction::PIA_JUMP };
            if (!jumpEnded_ && actions_[j] && actionSince_[j] < 1.f)
                kinematicController_->SetGravity(GRAVITY * Sqrt(actionSince_[j]));
            else
                kinematicController_->SetGravity(GRAVITY);
        }

        if (kinematicController_->CanJump())
            jumpEnded_ = false;

        if (hold_ && move.LengthSquared() > .1f)
        {
            if (hold_->GetType() == "RedHerring")
                hold_->GetNode()->SetWorldDirection(move.NormalizedOrDefault(node_->GetDirection()));
        }
    }
}

void Tux::ClingCheck(const Vector3& move)
{
    clinging_ = false;
    if (IsStunned())
        return;

    if (!kinematicController_->CanJump())
    {
        if (kinematicController_->GetLinearVelocity().y_ < .0f && move.Length() > .1f)
        {
            Vector3 wallNormal{};
            PhysicsWorld* physicsWorld{ GetScene()->GetComponent<PhysicsWorld>() };
            PhysicsRaycastResult result{};
            const Vector3 nodePos{ node_->GetWorldPosition() };
            float fromWall{};

            for (int side: { 0, -1, 1 })
            {
                if (clinging_ && node_->GetWorldDirection().Angle(wallNormal) < .1f)
                    break;

                for (bool high: { false, true })
                {
                    const Vector3 sideMove{ move.CrossProduct(Vector3::UP).ProjectOntoPlane(Vector3::UP).Normalized() };
                    const Vector3 shift{ .1f * side * sideMove };
                    const Ray ray{ AvoidSeams(nodePos + Vector3::UP * (.5f + .75f * high) + shift), move };

                    if (physicsWorld->RaycastSingle(result, ray, .55f, LAYER(L_STATIC)))
                    {
                        if (result.body_ && !result.body_->IsTrigger() && Equals(result.normal_.y_, 0.f, .1f))
                        {
                            fromWall = (nodePos - result.position_).ProjectOntoPlane(Vector3::UP).DotProduct(result.normal_);

                            if (fromWall < .4875f)
                            {
                                if (!high)
                                    wallNormal = result.normal_.ProjectOntoPlane(Vector3::UP).Normalized();
                                else if (result.normal_.Angle(wallNormal) < 1.f)
                                    clinging_ = true;
                            }
                        }
                    }
                }
            }

            if (clinging_)
            {
                node_->LookAt(nodePos + wallNormal, Vector3::UP);
                kinematicController_->Warp(node_->GetWorldPosition() + (wallNormal * (.41f - fromWall)));

                animCtrl_->StopLayer(2, .1f);
                animCtrl_->PlayExclusive("Animations/Cling.ani", 1, true, .05f);
                animCtrl_->SetStartBone("Animations/Cling.ani", "Hips");

                sinceCling_ = 0.f;
            }
        }
    }
    else
    {
        sinceCling_ = Max(sinceCling_, .05f);
    }

    if (!IsClinging() && animCtrl_->IsPlaying("Animations/Cling.ani"))
        animCtrl_->StopLayer(1, .1f);

}

Vector3 Tux::AvoidSeams(const Vector3& position) const
{
    Vector3 avoided{ position };
    constexpr float margin{ .05f };
    float val{ position.y_ };

    const float m{ Mod(val, 1.f) };
    if (m < margin)
        val = Floor(val) + margin;
    else if (m > 1.f - margin)
        val = Ceil(val) - margin;

    avoided.y_ = val;

    return avoided;
}

void Tux::PostUpdate(float timeStep)
{
    for (const String& side: { "R", "L" })
    {
        const String ani{ "Animations/Cast." + side + ".ani" };
        if (animCtrl_->IsAtEnd(ani))
        {
            const float castWeight{ animCtrl_->GetWeight(ani) };
            animCtrl_->SetWeight(ani, Lerp(castWeight, 0.f, Min(1.f, (5.5f - castWeight) * timeStep)));

            if (castWeight < .23f)
            {
                animCtrl_->Stop(ani, 1.f);

                if (side == "R")
                    leftCast_ = false;
            }
        }
    }

    if (!sliding_)
        node_->SetPosition(node_->GetPosition().Lerp(kinematicController_->GetPosition(), Min(1.f, 34.f * timeStep)));
    else
    {
        slider_->move_ = move_;

        if (IsUnderwater())
            slider_->move_.y_ = actions_[PIA_JUMP] - actions_[PIA_SLIDE];
    }
}

void Tux::Reset(const Vector3 position, const Quaternion rotation, bool fillHealth)
{
    if (sliding_)
        EndSliding();

    if (IsStunned())
        EndStun(false);

    if (fillHealth)
    {
        SetHat("");
        GetSubsystem<HealthIndicator>()->SetHealth(4, false);
    }

    GetSubsystem<OxygenIndicator>()->SetShowOxygen(false);
    graphicsNode_->GetChild("Bubbles", true)->RemoveAllComponents();

    walkSpeed_ = 0.f;
    jumpEnded_ = true;
    hold_ = nullptr;

    node_->SetPosition(position);
    node_->SetRotation({ 23.5f + rotation.YawAngle(), Vector3::UP });
    kinematicController_->SetLinearVelocity(Vector3::ZERO);
    kinematicController_->SetWalkDirection(Vector3::ZERO);
    kinematicController_->Warp(position);

    fire_->RemoveAllParticles();
    animCtrl_->StopLayer(1);
    animCtrl_->StopLayer(2);

    ResetInput();
}

void Tux::HandlePostRenderUpdate(const StringHash, VariantMap& /*eventData*/)
{
//    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
//    collisionShape_->DrawDebugGeometry(debug, false);
//    if (sliding_)
//        slider_->collisionShape_->DrawDebugGeometry(debug, false);

    if (hat_->GetModel() && hat_->GetModel()->GetName().Contains("HardHat"))
    {
        const String selectedItem{ GetSubsystem<Inventory>()->GetSelected() };
        if (CanUse(GetSubsystem<Inventory>()->GetSelected()))
        {
            Color col{ Color::TRANSPARENT_BLACK };
            if (selectedItem == "Bricks")
                col = Color::AZURE;
            else if (selectedItem == "Tools")
                col = Color::ORANGE;

            const IntVector3 ac{ HardHat::GetAllowedCoords() };
            if (col.a_ != 0.f && ac.x_ != M_MAX_INT)
            {
                const bool chisel{ col.b_ == 0 };
                DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
                const float halfSize{ .425f + .075f * chisel };
                const BoundingBox checkBounds{ ac - Vector3::ONE * halfSize, ac + Vector3::ONE * halfSize };
                debug->AddBoundingBox(checkBounds, col, true);
                debug->AddBoundingBox(checkBounds, col.Transparent(.23f), false);
                debug->AddBoundingBox(checkBounds, col.Transparent(.05f), false, true);
            }
        }
    }

    if (!burnt_ && IsStunned())
        return;

//    float timeStep{ eventData[PostRenderUpdate::P_TIMESTEP].GetFloat() };

    for (bool left: { true, false })
    {
        Node* eye{ graphicsNode_->GetChild(left ? "Eye.L" : "Eye.R", true) };

        //Blink
        for (bool top: { true, false })
        {
            const String boneName{ String{ "EyeLid" } + (top ? "Top" : "Bottom") + (left ? ".L" : ".R")};
            Node* eyelid{ graphicsNode_->GetChild(boneName, true) };
        }

        eye->LookAt(graphicsNode_->GetWorldPosition() + graphicsNode_->GetWorldDirection() * 42.0f);

        Node* cheek{ graphicsNode_->GetChild(left ? "Cheek.L" : "Cheek.R", true) };
        cheek->SetScale(Vector3(1.0f, Sin(GetScene()->GetElapsedTime() * 0.23f) * 0.42f + 1.0f, 1.0f));
    }
}

void Tux::FixedPostUpdate(float timeStep)
{
    if (sliding_)
        return;

    if (movingData_[0].IsSameNode(movingData_[1]))
    {
        Matrix3x4 delta{ movingData_[0].transform_ * movingData_[1].transform_.Inverse() };

        // Add delta
        Vector3 kPos;
        Quaternion kRot;
        kinematicController_->GetTransform(kPos, kRot);
        Matrix3x4 matKC{ kPos, kRot, Vector3::ONE };

        // Update KCC
        matKC = delta * matKC;
        kinematicController_->SetTransform(matKC.Translation(), matKC.Rotation());
        // Update node yaw
        const float yaw{ delta.Rotation().YawAngle() };
        node_->Yaw(yaw);
    }

    // Update node position
    node_->SetWorldPosition(kinematicController_->GetPosition());

    // Shift and clear
    movingData_[1] = movingData_[0];
    movingData_[0].node_ = nullptr;
}

void Tux::HandleNodeCollision(const StringHash /*eventType*/, VariantMap& eventData)
{
    using namespace NodeCollision;

    RigidBody* otherBody{ static_cast<RigidBody*>(eventData[P_OTHERBODY].GetPtr()) };
    Node* otherNode{ static_cast<Node*>(eventData[P_OTHERNODE].GetPtr()) };

    if (otherNode->HasTag("Lava"))
    {
        PhysicsRaycastResult result{};
        if (GetScene()->GetComponent<PhysicsWorld>()->RaycastSingle(result, { node_->GetWorldPosition() + Vector3::UP, Vector3::DOWN }, 2.f, LAYER(L_STATIC))
            && result.body_->GetNode() == otherNode)
            Burn();
    }
    else
    {
        MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };
        bool flatContact{ false };
        while (!contacts.IsEof())
        {
            // Read position and normal
            contacts.ReadVector3();
            const Vector3 normal{ contacts.ReadVector3() };
            if (normal.Angle(Vector3::UP) <= kinematicController_->GetMaxSlope())
                flatContact = true;
        }

        // Possible moving platform trigger volume
        if (flatContact && otherBody->IsTrigger())
            NodeOnMovingPlatform(otherNode);
    }
}

void Tux::NodeOnMovingPlatform(Node* node)
{
    if (!node || !kinematicController_->OnGround() || !node->HasTag("MovingPlatform"))
        return;

    movingData_[0].node_ = node;
    movingData_[0].transform_ = node->GetWorldTransform();
}

void Tux::HandleAnimationTrigger(const StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (kinematicController_->OnGround())
        PlaySample("Hit.wav", 0.f, .2f + .3f * actions_[PIA_RUN]);
}
