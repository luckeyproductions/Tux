/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../items/hardhat.h"
#include "blockmap.h"

#include "block.h"


void Block::RegisterObject(Context* context)
{
    context->RegisterFactory<Block>();
}

Block::Block(Context* context): Component(context)
{
}

void Block::Initialize(Model* model, const PODVector<Material*>& materials, Model* collisionMesh, bool collider)
{
    if (model == nullptr && collisionMesh == nullptr)
    {
        Remove();
        return;
    }

    if (collider)
    {
        collider_ = node_->GetParent()->CreateComponent<CollisionShape>();
        collider_->SetMargin(.025f);

        String modelName{ model->GetName() };
        if (!collisionMesh)
        {
            modelName.Insert(modelName.Length() - 4, "_COLLISION");
            collisionMesh = RES_NO(Model, modelName);
        }

        if (collisionMesh)
        {
            collider_->SetTriangleMesh(collisionMesh, 0, Vector3::ONE, Vector3::DOWN * collider_->GetMargin() + node_->GetPosition(), node_->GetRotation());

            if (modelName.Contains("Base") || modelName.Contains("Trunk"))
                HardHat::AddFoundation(VectorRoundToInt(node_->GetWorldPosition()));
        }
        else if (!modelName.Contains("GrassBlades"))
        {
            collider_->SetBox(Vector3::ONE, node_->GetPosition() + Vector3::DOWN * collider_->GetMargin());
            HardHat::AddFoundation(VectorRoundToInt(node_->GetWorldPosition()));
        }
        else
        {
            collider_->Remove();
            collider_ = nullptr;
        }
    }

    unsigned materialHash{ 0u };
    for (Material* m: materials)
        materialHash |= m->GetName().ToHash();

    unsigned blockHash{ model->GetNameHash().ToHash() ^ materialHash };

    HashMap<unsigned, StaticModelGroup*>& blockGroups{ node_->GetParent()->GetComponent<BlockMap>()->GetBlockGroups() };
    if (!blockGroups.Contains(blockHash) || blockGroups[blockHash]->GetScene() != GetScene())
    {
        blockGroups[blockHash] = GetScene()->CreateComponent<StaticModelGroup>();
        blockGroups[blockHash]->SetModel(model);
        blockGroups[blockHash]->SetCastShadows(true);
        if (materials.Size() == 1)
            blockGroups[blockHash]->SetMaterial(materials.Front());
        else for (unsigned m{ 0u }; m < materials.Size(); ++m)
            blockGroups[blockHash]->SetMaterial(m, materials.At(m));
    }

    blockGroups[blockHash]->AddInstanceNode(node_);
}
