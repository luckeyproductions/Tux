/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DUNGEON_H
#define DUNGEON_H

#include "../mastercontrol.h"

class BlockMap : public Component
{
    DRY_OBJECT(BlockMap, Component);
    friend class InputMaster;

public:
    static void RegisterObject(Context* context);
    BlockMap(Context *context);

    const String& GetMusic() const { return music_; }
    Vector3 GetBlockSize() const { return blockSize_; }
    const IntVector3& GetMapSize() const { return mapSize_; }

    void OnNodeSet(Node* node) override;
    void LoadXMLFile(XMLFile* blockMap, bool collision = true);
    bool LoadXML(const XMLElement& blockMap) override;

    HashMap<unsigned, StaticModelGroup*>& GetBlockGroups() { return blockGroups_; }

    void Clear();
    bool IsColliding() const { return collision_; }

    bool HasWater() const { return liquidNode_->IsEnabledSelf() && !liquidNode_->HasTag("Lava"); }
    float GetWaterLevel() const { return liquidNode_->GetWorldPosition().y_; }
    bool IsUnderwater(const Vector3& position) const { return HasWater() && ToSurface(position) > 0.f; }
    float ToSurface(const Vector3& position) const { return GetWaterLevel() - position.y_; }

    HashSet<IntVector3> GetUnoccupied(bool underwater = false) const;

private:
    String music_;
    Zone* zone_;
    HashMap<unsigned, StaticModelGroup*> blockGroups_;

    IntVector3 mapSize_;
    Vector3 blockSize_;
    RigidBody* rigidBody_;
    Node* infiniteGrassNode_;
    Node* liquidNode_;
    CollisionShape* groundCollider_;
    bool collision_;
    HashSet<IntVector3> occupied_;
};

#endif // DUNGEON_H
