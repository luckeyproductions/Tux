/* Edddy
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TILE_H
#define TILE_H

#include "heightprofile.h"
#include "cyberplasm/witch.h"

#include "block.h"

class Tile: public Block
{
    DRY_OBJECT(Tile, Block);

public:
    Tile(Context* context);

    void Initialize(const HeightProfile& profile, Material* material = nullptr, bool collider = true);

protected:
    void OnNodeSet(Node* node) override;

private:
    void generateTile();
    Witch::Rune cornerRune(int index, bool global) const;

    StaticModel* tileModel_;
    HeightProfile profile_;
};

#endif // TILE_H
