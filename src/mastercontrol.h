/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define DEFAULT_FOG_COLOR Color::CHARTREUSE.Lerp(Color::CYAN, .17f).Lerp(Color::BLACK, 0.23f)

namespace Dry {
class Node;
class Scene;
}

class Settings;
class Player;
class Tux;

enum Layer{ L_STATIC = 1, L_PLAYER, L_ITEM, L_PROJECTILE, L_ENEMY, L_WHIPGRIP };

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(Context* context);
    String GetStoragePath() const { return storagePath_; }

    void Setup() override;
    void Start() override;
    void Stop() override;
    void Exit();

private:
    void SetupRenderer();
    String HandleArguments();

    String storagePath_;
};

#endif // MASTERCONTROL_H
