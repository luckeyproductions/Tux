/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "ui/sardinerunmenu.h"

#include "scores.h"

Scores::Scores(Context* context): Object(context),
    scoreFile_{ context_->CreateObject<JSONFile>() }
{
    if (GetSubsystem<FileSystem>()->FileExists(SCOREPATH))
        scoreFile_->LoadFile(SCOREPATH);
}

PODVector<int> Scores::GetScores(const String& mapName)
{
    const String level{ ScoreLevelName(mapName) };
    PODVector<int> scores{};
    const JSONValue& root{ scoreFile_->GetRoot() };

    if (root.Contains(level))
    {
        const JSONArray& array{ root.Get(level).GetArray() };
        for (unsigned i{ 0u }; i < array.Size(); ++i)
            scores.Push(array.At(i).GetInt());
    }

    return scores;
}

void Scores::AddScore(const String& mapName, int score)
{
    const String level{ ScoreLevelName(mapName) };
    JSONValue& root{ scoreFile_->GetRoot() };

    PODVector<int> scores{ GetScores(level) };
    scores.Push(score);
    Sort(scores.Begin(), scores.End());

    JSONArray array{};
    for (int s: scores)
        array.Push(JSONValue{ s });

    root.Set(level, array);

    Save();
    GetSubsystem<GUI>()->GetSardineRunMenu()->UpdateScoreList(level);
}

bool Scores::Save()
{
    if (!scoreFile_->SaveFile(SCOREPATH))
        return false;
    else
        return true;
}

String Scores::ScoreLevelName(const String& mapName)
{
    unsigned start{ mapName.Find("Maps/") };
    if (start == String::NPOS)
        start = 0u;
    else
        start += 5u;

    return ReplaceExtension(mapName.Substring(start), "");
}
