/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include <bitset>
//#include "pathfinder.h"

#include "sceneobject.h"

#define INPUTMASTER GetSubsystem<InputMaster>()

class Controllable: public SceneObject
{
    DRY_OBJECT(Controllable, SceneObject);

public:
    Controllable(Context* context);
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;

    void SetMove(Vector3 move);
    void SetAim(Vector3 aim);
    void SetActions(std::bitset<4> actions);
    virtual void HandleAction(int actionId);

    Vector3 GetMove() const { return move_; }
    Vector3 GetAim()  const { return aim_; }
    bool Bounce() const;

    virtual void ClearControl();
    virtual void Think(float timeStep) {}

    Node* GetGraphicsNode() const { return graphicsNode_; }
    Vector3 GetLinearVelocity() const { return rigidBody_->GetLinearVelocity(); }

    Player* GetPlayer();

protected:
    bool controlled_;
    Vector3 move_;
    Vector3 aim_;
    float thrust_;
    float maxSpeed_;
    float maxPitch_;
    float minPitch_;

    std::bitset<4> actions_;
    HashMap<int, float> actionSince_;
    HashMap<int, float> actionInterval_;

    Node* graphicsNode_;
    AnimatedModel* model_;
    RigidBody* rigidBody_;
    CollisionShape* collisionShape_;
    AnimationController* animCtrl_;

    void ResetInput() { move_ = aim_ = Vector3::ZERO; actions_.reset(); }
    void ClampPitch(Quaternion& rot);

    void AlignWithVelocity(float timeStep);
    void AlignWithMovement(float timeStep);

    virtual void EndAction(int actionId) {}
};

// Moving platform data
struct MovingData
{
    MovingData():
        node_{ nullptr }
    {
    }

    MovingData& operator =(const MovingData& rhs)
    {
        node_ = rhs.node_;
        transform_ = rhs.transform_;
        return *this;
    }

    bool IsSameNode(const MovingData& rhs)
    {
        return (node_ && node_ == rhs.node_);
    }

    Node* node_;
    Matrix3x4 transform_;
};

#endif // CONTROLLABLE_H
