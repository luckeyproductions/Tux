/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../effect/poof.h"

#include "door.h"

Door::Door(Context* context): Environ(context)
{
}

void Door::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetName("Door");
    model_ = node_->CreateComponent<AnimatedModel>();
}

void Door::Initialize(Model* model, const PODVector<Material*>& materials, Model* collisionMesh)
{
    Environ::Initialize(model, materials, collisionMesh);

    rigidBody_->SetCollisionLayerAndMask(LAYER(L_STATIC), M_MAX_UNSIGNED);
    model_->SetModel(model);
    model_->SetCastShadows(true);

    for (unsigned m{ 0u }; m < materials.Size(); ++m)
        model_->SetMaterial(m, materials.At(m));
}

void Door::Stretch()
{
    const bool trapDoor{ IsTrapdoor() };
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    Vector2 size{};
    Vector2 offset{};

    for (int d{ 0 }; d < 4; ++d)
    {
        Ray ray{ node_->GetWorldPosition(), Vector3::ZERO };

        switch (d)
        {
        case 0: ray.direction_ =  node_->GetWorldRight(); break;
        case 1: ray.direction_ = -node_->GetWorldRight(); break;
        case 2: ray.direction_ = (trapDoor ? -node_->GetWorldDirection() :  node_->GetWorldUp()); break;
        case 3: ray.direction_ = (trapDoor ?  node_->GetWorldDirection() : -node_->GetWorldUp()); break;
        default: break;
        }

        PhysicsRaycastResult result{};
        physics->RaycastSingle(result, ray, 10.f, LAYER(L_STATIC));
        const float dist{ result.distance_ };
        if (dist < M_INFINITY)
        {
            if (d < 2)
            {
                size.x_ += dist;
                offset.x_ += d % 2 ? -dist : dist;
            }
            else
            {
                size.y_ += dist;
                offset.y_ += d % 2 ? -dist : dist;
            }
        }
        else
        {
            node_->Remove();
            return;
        }
    }

    size = VectorRound(size);
    offset = .25f * VectorRound(offset * 2.f);

    Vector3 box;
    if (trapDoor)
    {
        node_->Translate({ offset.x_, 0.f, offset.y_ });
        box = { size.x_, .2f, size.y_ };
        collider_->SetBox(box);
    }
    else
    {
        node_->Translate({ offset.x_, offset.y_, 0.f });
        box = { size.x_, size.y_, .2f };
        collider_->SetBox(box);
    }

    const String modelName{};
    SharedPtr<Model> m{ model_->GetModel()->Clone(GetFileName(model_->GetModel()->GetName())) };
    m->SetBoundingBox({ -.5f * box, .5f * box });
    model_->SetModel(m);
    model_->SetMorphWeight(0, size.x_ - 1.f);
    model_->SetMorphWeight(1, size.y_ - 1.f);
}

void Door::Open()
{
    Node* poofNode{ GetScene()->CreateChild("Poof") };
    poofNode->SetWorldPosition(node_->GetWorldPosition());
    poofNode->CreateComponent<Poof>();

    if (!IsTrapdoor())
        poofNode->SetWorldRotation(Quaternion{ -90.f, node_->GetWorldRight() } * node_->GetWorldRotation());

    PlaySample("Door.wav", 0.f, 1.7f, false);
    node_->Remove();
}

