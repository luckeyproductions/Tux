/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WHIPGRIP_H
#define WHIPGRIP_H

#include "environ.h"


class WhipGrip: public Environ
{
    DRY_OBJECT(WhipGrip, Environ);

public:
    WhipGrip(Context* context);

    void Initialize(Model* model = nullptr, const PODVector<Material*>& materials = {}, Model* collisionMesh = nullptr) override;
    Constraint* GetConstraint() const { return constraint_; }

protected:
    void OnNodeSet(Node* node) override;

private:
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    StaticModel* model_;
    Constraint* constraint_;
    Vector3 swingAxis_;
};

#endif // WHIPGRIP_H
