/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DOOR_H
#define DOOR_H

#include "environ.h"

class Door: public Environ
{
    DRY_OBJECT(Door, Environ);

public:
    Door(Context* context);

    void Initialize(Model* model = nullptr, const PODVector<Material*>& materials = {}, Model* collisionMesh = nullptr) override;
    void Stretch();
    void Open();

    bool IsTrapdoor() const { return model_->GetModel()->GetName().Contains("Trap"); }

protected:
    void OnNodeSet(Node* node) override;

private:
    AnimatedModel* model_;
};

#endif // DOOR_H
