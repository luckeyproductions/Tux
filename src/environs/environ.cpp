/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../blockmap/blockmap.h"

#include "environ.h"

Environ::Environ(Context* context): SceneObject(context),
    rigidBody_{ nullptr },
    collider_{ nullptr }
{
}

void Environ::Initialize(Model* model, const PODVector<Material*>& /*materials*/, Model* collisionMesh)
{
    if (model == nullptr && collisionMesh == nullptr)
    {
        node_->Remove();
        return;
    }

    if (MapHasCollision())
    {
        rigidBody_ = node_->CreateComponent<RigidBody>();
        rigidBody_->SetCollisionLayer(LAYER(L_STATIC));
        rigidBody_->SetCollisionMask(LAYER(L_PLAYER) | LAYER(L_PROJECTILE) | LAYER(L_ENEMY));

        collider_ = node_->CreateComponent<CollisionShape>();
        collider_->SetMargin(.025f);

        if (!collisionMesh)
        {
            String modelName{ model->GetName()} ;
            modelName.Insert(modelName.Length() - 4, "_COLLISION");
            collisionMesh = RES_NO(Model, modelName);
        }

        if (collisionMesh)
            collider_->SetTriangleMesh(collisionMesh, 0, Vector3::ONE, Vector3::DOWN * collider_->GetMargin());
    }
}

void Environ::OnSetEnabled() { Component::OnSetEnabled(); }
void Environ::OnNodeSet(Node* node) {}
void Environ::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Environ::OnMarkedDirty(Node* node) {}
void Environ::OnNodeSetEnabled(Node* node) {}

bool Environ::MapHasCollision()
{
    Node* parent{ node_->GetParent() };
    if (BlockMap* bm{ parent->GetComponent<BlockMap>() })
        return bm->IsColliding();

    return true;
}

bool Environ::Save(Serializer& dest) const { return Component::Save(dest); }
bool Environ::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Environ::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Environ::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Environ::GetDependencyNodes(PODVector<Node*>& dest) {}
void Environ::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
