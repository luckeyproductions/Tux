include(src/Tux.pri)

TARGET = tux

LIBS += \
    $$(DRY_HOME)/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    $$(DRY_HOME)/include \
    $$(DRY_HOME)/include/Dry/ThirdParty

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/tux/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = tux.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = tux.desktop
    INSTALLS += desktop

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
