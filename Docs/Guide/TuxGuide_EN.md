![Tux!](images/Logo.png){width=320px}

A 3D platform game featuring the famous Linux mascot.

# Guide

Welcome to the guide for Tux! — Search for Sardinia. 

Herein you will find everything you need to know before playing the game.

[[_TOC_]]

## Story

Tux is a fairy penguin living a quiet life on the shores of Denmark, Australia together with his wife and two children. With the ocean running out of sardines however, Tux has to travel and search the oddest places to find enough fish to feed his family. Somehow there's plenty scattered everywhere on land, which appears to form something of a trail. But where does it lead? Might the empty oceans have something to do with the evil profit-driven Setagllib, his fish farms and the research that goes on there into lab-grown fish?

## Features

Tux has many inherent abilities, for example he can run, jump, and slide. There are also [hats](#hats) and [single-use](#single-use) items that can help him during his adventures.

## How to make Tux Move

### Controller

Controller Button | Action |
|:---:|:---:|
| **Left Stick** | Move around |
| **Right Stick** | Turn camera |
| **A Button** | Jump |
| **B Button** | Slide |
| **X Button** | Run/Walk |
| **Y Button** | Use |
| **Right shoulder** | Next item |
| **Left shoulder** | Previous item |
| **Start Button** | Pause |


### Keyboard

| Key | Action |
|:---:|:---:|
| **WASD** | Move around |
| **Shift** | Run/Walk |
| **Space** | Jump |
| **Alt** | Slide |
| **Ctrl** | Use |
| **]**/**+** | Next item |
| **[**/**-** | Previous item |
| **P**/**Esc** | Pause |
| **Esc** | UnPause |

### Mouse

| Mouse Button | Action |
|:---:|:---:|
| **Left Button** | Jump |
| **Right Button** | Slide |
| **Middle Button** | Use |

The mouse also controls the camera which is useful for lining up jumps and attacks!

## Items

### Hats

#### Football Helmet
![Football Helmet](images/Helmet.png){width=128px}  

Become _Quarterback Tux_ and deal damage in otherwise hurtful situations through sliding.

#### Fedora
![Fedora](images/Fedora.png){width=128px} ![bullwhip](images/Bullwhip.png){width=64px}  

Turns our hero into _Indie Tux_, equipped with a bullwhip that functions as a stunning grappling hook.

#### Hard Hat
![Hard Hat](images/HardHat.png){width=128px} ![Bricks](images/Bricks.png){width=64px}   ![Hammer & Chisel](images/Tools.png){width=64px}    

Wearing this hat you become _Builder Tux_, providing five sets of brick as well as a hammer and chisel with which bricks can be retrieved for reuse. Note that the first brick must be laid on flat sturdy ground and bricks can only be removed if it would not cause structural collapse.  
Also functions like a football helmet.

#### Wizard Hat
![Wizard Hat](images/WizardHat.png){width=128px} ![Fire](images/FireSpell.png){width=64px}   ![Ice](images/IceSpell.png){width=64px} 

As _Wizard Tux_ you can freeze and scorch your enemies using fire and ice spells. The ice spell can also be used to temporarily turn water and lava into solid platforms.

#### SCUBA Mask
![SCUBA Mask](images/ScubaMask.png){width=128px}

Allows breathing underwater indefinitely.

### Single-use

#### Keys
![Key](images/Key.png)  

Single use skeleton key that fits any keyhole.

#### Red Herring
![Red Herring](images/Herring.png)

A brightly coloured wind-up toy shaped like a fish; the perfect decoy that distracts enemies to a foolish chase, allowing Tux to sneak past.


## Enemies

### Land

#### Zombie snail

![Zombie Snail](images/ZombieSnail.png)

Unthinking gastropod going back and forth between walls/cliffs. Tux doesn't need a power up to defeat, but the player needs to bounce on the top of the shell twice.

#### Bullhorn

![Bullhorn](images/Bullhorn.png){width=280px}

Orthogonally storming highland cattle, male.

## Making levels

Using [Edddy](https://luckeyproductions.itch.io/edddy) it is possible to make more levels for the game.