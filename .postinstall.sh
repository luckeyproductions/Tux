#!/bin/sh

sudo chown -R $USER ~/.local/share/luckey/tux/
sudo chown $USER ~/.local/share/icons/tux.svg
update-icon-caches ~/.local/share/icons/
