#!/bin/sh

cd `dirname $0`;
if [ ! -d Dry ]
then
    git clone https://gitlab.com/luckeyproductions/Dry.git
    cd Dry 
else
    cd Dry
    git checkout master
    git pull
fi

./script/installreq.sh
./script/cmake_clean.sh ./build
./script/cmake_generic.sh ./build \
    -DDRY_ANGELSCRIPT=0 -DDRY_2D=0 \
    -DDRY_SAMPLES=0 -DDRY_TOOLS=0
cd build; make
cd ../..
